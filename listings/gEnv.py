import ConfigParser
import logging
import logging.handlers
import threading
from contextlib import contextmanager

import redis
from sqlalchemy import event, util

try:
    from sqlalchemy.orm import ScopedSession
except ImportError:
    from sqlalchemy.orm.scoping import ScopedSession  # SQLAlchemy 0.8.x

import model


class GlobalEnvironment(object):
    """
        Provide access to the external resources (e.g. DB, Redis, logger instance)
    """

    def __init__(self, configfp):
        self.__parse_config(configfp)
        self.__setup_logging()
        self._smaker_lock = threading.Lock()

        self.cache = redis.Redis(
            host=self.settings["cache"]["host"],
            port=self.settings["cache"]["port"],
            db=self.settings["cache"]["db"],
            password=self.settings["cache"]["password"]
        )

    def __parse_config(self, configfp):
        """
            Read settings from a file-like object (e. g. opened file)
        """

        self.settings = {}

        parser = ConfigParser.SafeConfigParser({"pooling": False})
        parser.readfp(configfp)

        self.settings["gearman"] = {
            "hosts": [host for _, host in parser.items("gearman")],
        }

        self.settings["api"] = {
            "session_ttl": parser.getint("api", "session_ttl"),
            "permissions_path": parser.get("api", "permissions_path"),
            "dispatcher_port": parser.getint("api", "dispatcher_port")
        }

        self.settings["db"] = {
            "host": parser.get("db", "host"),
            "port": parser.getint("db", "port"),
            "name": parser.get("db", "name"),
            "user": parser.get("db", "user"),
            "password": parser.get("db", "password"),
            "pooling": parser.getboolean("db", "pooling")
        }

        self.settings["cache"] = {
            "host": parser.get("cache", "host"),
            "port": parser.getint("cache", "port"),
            "db":   parser.getint("cache", "db"),
            "password": parser.get("cache", "password"),
        }

        self.settings["logging"] = {
            "server": parser.get("logging", "server"),
            "path": parser.get("logging", "path"),
            "maxsize": parser.getint("logging", "maxsize")
        }

    def __setup_logging(self):
        socket_handler = logging.handlers.SocketHandler(self.logging_server, logging.handlers.DEFAULT_TCP_LOGGING_PORT)

        logging.getLogger().addHandler(socket_handler)

        log_levels = {
            "api": logging.DEBUG,
            "dispatcher": logging.DEBUG,
            "guardian": logging.DEBUG,
            "sender": logging.DEBUG,

            # WARNING log level is choosen because sqlalchemy is known to emit
            # some non-pickable messages during mappers initialization that
            # lead to significant perfomance drops if SocketHandler is used.
            # In model.py:init_orm() we can set the log level we need
            "sqlalchemy": logging.WARNING
        }

        for logname, level in log_levels.iteritems():
            l = logging.getLogger(logname)
            l.setLevel(level)

    @contextmanager
    def get_db_session(self, automatic_audit=False, audit_session_info={}):
        """
            Get an SQLAlchemy DB session

            Returns a context manager which always closes the session
            on exit. If there were no exceptions, the transaction is committed,
            otherwise - it's automatically rollbacked.

            Usage:
                with gEnv.get_db_session() as s:
                    s.query(...)

            One can pass the 'automatic_audit' argument equal to True to enable
            basic automatic adding of audit table entries (changing of ORM mapped
            DB entities will be handled). In order to make audit entries more
            informative you should also pass the 'audit_session_info' argument
            (the one you received from the LightWeightWorker when the API call
             was dispatched)
        """

        # sessiomaker is created lazily because we use SQLAlchemy schema
        # reflection facitities and don't want to get the PostgreSQL server
        # overloaded on service restart (60+ processes would try to load
        # the metadata simultaneously)
        with self._smaker_lock:
            if getattr(self, "sessionmaker", None) is None:
                sessionmaker = model.get_sessionmaker(
                    host=self.settings["db"]["host"],
                    port=self.settings["db"]["port"],
                    dbname=self.settings["db"]["name"],
                    user=self.settings["db"]["user"],
                    password=self.settings["db"]["password"],
                    pooling=self.settings["db"]["pooling"]
                )
                self.sessionmaker = ScopedSession(sessionmaker)

        session = self.sessionmaker()
        try:
            if automatic_audit:
                session.audit_session_info = audit_session_info
                event.listen(session, "after_flush", after_flush_hook)

            yield session
        except:
            session.rollback()
            raise
        else:
            session.commit()
        finally:
            session.close()

    def get_cache_connection(self):
        return self.cache

    def get_logger(self, name="api"):
        return logging.getLogger(name)

    @property
    def gearman_hosts(self):
        return self.settings["gearman"]["hosts"]

    @property
    def session_ttl(self):
        return self.settings["api"]["session_ttl"]

    @property
    def permissions_path(self):
        return self.settings["api"]["permissions_path"]

    @property
    def dispatcher_port(self):
        return self.settings["api"]["dispatcher_port"]

    @property
    def logging_server(self):
        return self.settings["logging"]["server"]

    @property
    def logging_path(self):
        return self.settings["logging"]["path"]

    @property
    def logging_maxbytes(self):
        return self.settings["logging"]["maxbytes"]


def after_flush_hook(session, flush_context):
    audit_entries = []

    audit_entries.extend(_audit_new_entries(session, flush_context, session.audit_session_info))
    audit_entries.extend(_audit_updated_entries(session, flush_context, session.audit_session_info))
    audit_entries.extend(_audit_deleted_entries(session, flush_context, session.audit_session_info))

    session.add_all(audit_entries)


def _audit_new_entries(session, flush_context, audit_session_info={}):
    audit_entries = []

    for entry in session.new:
        # prevent recursive adding of audit entries
        if entry.__class__.__table__ is model.Audit.__table__:
            continue
        # prevent adding of audit entries for `system` tables
        if entry.__class__.__table__.name == "data_update":
            continue

        for col, value in entry._sa_instance_state.dict.items():
            # don't touch sqlalchemy internal values
            if col == "_sa_instance_state":
                continue

            # dirty hacks in order to handle references to other entities right...
            if hasattr(value, "_sa_instance_state"):
                col = "{0}_id".format(col)
                value = value.id

            if isinstance(value, list):
                if value and hasattr(value[0], "_sa_instance_state"):
                    continue
                else:
                    value = ''

            audit_entry = model.Audit(
                table_name=model.get_entity_table_name(entry),
                p_action="insert",
                fild_name=col,
                row_id=getattr(entry, "id", ""),
                old_value="",
                new_value=value,
                session_id=audit_session_info.get("id", ""),
                ip_address=audit_session_info.get("ip", ""),
                log_user_id=audit_session_info.get("user_id", 0)
            )
            audit_entries.append(audit_entry)

    return audit_entries


def _audit_updated_entries(session, flush_context, audit_session_info={}):
    audit_entries = []

    for entry in session.dirty:
        for col, oldvalue in entry._sa_instance_state.committed_state.items():
            # don't touch sqlalchemy internal values
            if col == "_sa_instance_state":
                continue

            # dirty hacks in order to handle references to other entities right...
            newvalue = getattr(entry, col)

            if newvalue == oldvalue:
                continue

            if oldvalue is util.symbol("PASSIVE_NO_RESULT"):
                oldvalue = ""

            if hasattr(oldvalue, "_sa_instance_state") or hasattr(newvalue, "_sa_instance_state"):
                col = "{0}_id".format(col)
                oldvalue = getattr(oldvalue, "id", "")
                newvalue = getattr(newvalue, "id", "")

            if isinstance(oldvalue, list):
                oldvalue = ", ".join([str(getattr(e, "id", e)) for e in oldvalue])
            if isinstance(newvalue, list):
                newvalue = ", ".join([str(getattr(e, "id", e)) for e in newvalue])

            audit_entry = model.Audit(
                table_name=model.get_entity_table_name(entry),
                p_action="update",
                fild_name=col,
                old_value=oldvalue,
                new_value=newvalue,
                row_id=getattr(entry, "id", ""),
                session_id=audit_session_info.get("id", ""),
                ip_address=audit_session_info.get("ip", ""),
                log_user_id=audit_session_info.get("user_id", 0)
            )
            audit_entries.append(audit_entry)

    return audit_entries


def _audit_deleted_entries(session, flush_context, audit_session_info={}):
    audit_entries = []

    for entry in session.deleted:
        for col in entry._sa_instance_state.unmodified:
            # dirty hacks in order to handle references to other entities right...
            oldvalue = getattr(entry, col)
            if hasattr(oldvalue, "_sa_instance_state"):
                col = "{0}_id".format(col)
                oldvalue = getattr(oldvalue, "id", "")

            if isinstance(oldvalue, list):
                oldvalue = ", ".join([str(getattr(e, "id", e)) for e in oldvalue])

            audit_entry = model.Audit(
                table_name=model.get_entity_table_name(entry),
                p_action="delete",
                fild_name=col,
                old_value=oldvalue,
                row_id=getattr(entry, "id", ""),
                session_id=audit_session_info.get("id", ""),
                ip_address=audit_session_info.get("ip", ""),
                log_user_id=audit_session_info.get("user_id", 0)
            )
            audit_entries.append(audit_entry)

    return audit_entries
