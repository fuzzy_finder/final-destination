"""
    This module provides means to get a DB session object
    and describes the mapping of model entites to DB tables.
"""

import re
import datetime
import hashlib
import threading

from sqlalchemy import create_engine, Table, Column, Integer, Text, DateTime, Numeric, String
from sqlalchemy.orm import mapper, sessionmaker, relationship, backref
from sqlalchemy.orm.mapper import configure_mappers
from sqlalchemy.orm.util import _is_mapped_class
from sqlalchemy.pool import NullPool, QueuePool
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.schema import Sequence
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.sql import select, func


INIT_ORM_LOCK = threading.Lock()


# this class is taken from sqlalchemy recipes
class DeclarativeReflectedBase(object):

    _mapper_args = []

    @classmethod
    def __mapper_cls__(cls, *args, **kw):
        cls._mapper_args.append((args, kw))

    @classmethod
    def prepare(cls, engine):
        while cls._mapper_args:
            args, kw = cls._mapper_args.pop()
            klass = args[0]

            if args[1] is not None:
                table = args[1]
                Table(table.name,
                      cls.metadata,
                      extend_existing=True,
                      autoload_replace=False,
                      autoload=True,
                      autoload_with=engine,
                      schema=table.schema)

            for c in klass.__bases__:
                if _is_mapped_class(c):
                    kw['inherits'] = c
                    break

            klass.__mapper__ = mapper(*args, **kw)

Base = declarative_base()


class Reflected(DeclarativeReflectedBase, Base):
    """
        A superclass for all model entities that are mapped
        to DB tables using the reflection-based approach
    """

    __abstract__ = True


# Some entites (e. g. TPs) can exist in different DB schemas (contexts) at the
# same time. For each of such entities is is neccessary to provide an
# independent mapping class declaration. For the sake of brevity the following
# approach is used - if entity being described can exist in several DB schemas,
# then:
#
#   1) declare an entity class that inherits from a built-in object class;
#   2) call the create_entity_for_schema() function to bind the described
#      entity to the appropriate DB schema;
#   3) call the get_entity_for_schema() function to get the appropriate
#      entity for a given DB schema name.
#
# Example:
#
#    class Tp(object):
#        __tablename__ = "tp"
#
#    ...
#
#    create_entity_for_schema(Tp, "routing")
#    create_entity_for_schema(Tp, "routing_cz")
#
#    ...
#
#    for tp in session.query(get_entity_for_schema(Tp, "routing")).all():
#        ...
#    for tp in session.query(get_entity_for_schema(Tp, "routing_cz")).all():
#        ...
#
# Entities which can exist only in one DB schema should inherit from the
# Reflected class directly and add an appropriate value for the 'schema' key
# in the __table_args__ class attribute dict
#
# Example:
#
#    class Colocation(Reflected):
#        __tablename__ = "colocation"
#        __table_args__ = {"schema": "routing"}
#

# schema name, entity name -> entity mapping
_MULTIPLE_SCHEMAS_ENTITIES = {}


def create_entity_for_schema(cls, schema="public", attrs={}, bases=()):
    """
        Create a separate entity class declaration for the given DB schema

        Parameters:
            cls    - a class for an entity that can exist in several DB schemas
            schema - a DB schema name
            attrs  - additional attributes to be set on the created entity class
            bases  - a tuple of classes to inherit from
    """

    newcls_name = "{0}_{1}".format(cls.__name__, schema)
    attrs.update({"__table_args__": {"schema": schema, "extend_existing": True}})
    newcls = type(newcls_name, (cls, Reflected) + bases, attrs)

    globals()[newcls_name] = newcls

    if schema not in _MULTIPLE_SCHEMAS_ENTITIES:
        _MULTIPLE_SCHEMAS_ENTITIES[schema] = {}

    _MULTIPLE_SCHEMAS_ENTITIES[schema][cls.__name__] = newcls


def get_entity_for_schema(cls, schema="public"):
    """
        Get the appropriate model entity for the given DB schema name
    """

    if schema not in _MULTIPLE_SCHEMAS_ENTITIES:
        return None

    return _MULTIPLE_SCHEMAS_ENTITIES[schema].get(cls.__name__, None)


def get_entity_table_name(entity, qualified=True):
    """
        Get the corresponding table name for given entity

        If `qualified` is equal to `True`, the table name gets a prefix of it's schema name
    """

    table = getattr(entity, "__table__", entity.__class__.__table__)

    if qualified:
        return "{0}.{1}".format(table.schema, table.name)
    else:
        return "{0}".format(table.name)


class ContextEntity(object):
    @hybrid_property
    def name_with_context(self):
        return "{0} ({1})".format(self.name, self.__class__.__table__.schema[-2:].upper())

    @name_with_context.expression
    def name_with_context_expression(cls):
        return cls.name + " (" + cls.__table__.schema[-2:].upper() + ")"


#
# Model entities mapped to DB tables (use DB schema as manual)
#


class Tp(object):

    @declared_attr
    def __tablename__(cls):
        return "tp"

    id = Column("id", Integer, primary_key=True)

    @hybrid_property
    def codecs_names(self):
        return [c.name for c in self.codecs]

    @codecs_names.expression
    def codecs_names_expression(cls):
        rel_codecs_tp = Reflected.metadata.tables["{0}.rel_codecs_tp".format(cls.__table__.schema)]

        q = select(
            [Codec.name],
            from_obj=[Codec.__table__.join(rel_codecs_tp)]
        ).where(
            rel_codecs_tp.c.tp_id == cls.id
        ).label("codecs")

        return func.array(q)

    @hybrid_property
    def codecs_list(self):
        return [(c.id, c.name) for c in self.codecs]

    @codecs_list.expression
    def codecs_list_expression(cls):
        rel_codecs_tp = Reflected.metadata.tables["{0}.rel_codecs_tp".format(cls.__table__.schema)]

        q = select(
            [func.row(Codec.id, Codec.name)],
            from_obj=[Codec.__table__.join(rel_codecs_tp)]
        ).where(
            rel_codecs_tp.c.tp_id == cls.id
        ).label("codecs_list")

        return func.array(q)


create_entity_for_schema(
    Tp,
    "routing",
    # this is how adding of relationship works
    {
        "id": Column("id", Integer, primary_key=True),

        "tarif": relationship(lambda: TpTarif),
        "balance": relationship(lambda: Balance)
    }
)


class TpIp(object):

    @declared_attr
    def __tablename__(cls):
        return "tp_ip"

    id = Column('id', Integer, primary_key=True)


class TpTarif(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "tp_tarif"

    __table_args__ = {"schema": "routing"}

    id = Column('id', Integer, primary_key=True)
    prices = relationship(lambda: TpPrice, backref="tp_tarif")
    currency = relationship(lambda: Currency)

    @hybrid_property
    def name_with_currency(self):
        return "{0} ({1})".format(self.name, self.currency.name_short)

    @name_with_currency.expression
    def name_with_curr_expr(cls):
        return cls.name + " (" + Currency.name_short + ")"


class Op(object):

    @declared_attr
    def __tablename__(cls):
        return "op"

    id = Column('id', Integer, primary_key=True)


create_entity_for_schema(
    Op,
    "routing",
    # this is how adding of relationship works
    {
        "id": Column("id", Integer, primary_key=True),

        "tarif": relationship(lambda: OpTarif),
        "balance": relationship(lambda: Balance)
    }
)


class OpIp(object):

    @declared_attr
    def __tablename__(cls):
        return "op_ip"

    id = Column('id', Integer, primary_key=True)


class OpTarif(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "op_tarif"

    id = Column('id', Integer, primary_key=True)
    currency = relationship(lambda: Currency)

    @hybrid_property
    def name_with_currency(self):
        return "{0} ({1})".format(self.name, self.currency.name_short)

    @name_with_currency.expression
    def name_with_curr_expr(cls):
        return cls.name + " (" + Currency.name_short + ")"

    __table_args__ = {"schema": "routing"}


class OpPrice(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "op_prices"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class Group(ContextEntity):

    @declared_attr
    def __tablename__(cls):
        return "groups"

    id = Column('id', Integer, primary_key=True)


class Dialpeer(ContextEntity):

    @declared_attr
    def __tablename__(cls):
        return "dialpeers"

    id = Column('id', Integer, primary_key=True)


create_entity_for_schema(
    Dialpeer,
    "routing",
    # this is how adding of relationship works
    {
        "id": Column("id", Integer, Sequence('dialpeers_id_seq', schema='routing'), primary_key=True),
    }
)


class RelOpGroup(object):

    @declared_attr
    def __tablename__(cls):
        return "rel_op_group"

    op_id = Column('op_id', Integer, primary_key=True)
    group_id = Column('group_id', Integer, primary_key=True)


class RelTpGroup(object):

    @declared_attr
    def __tablename__(cls):
        return "rel_tp_group"

    tp_id = Column('tp_id', Integer, primary_key=True)
    group_id = Column('group_id', Integer, primary_key=True)


class RelDialpeerGroup(object):

    @declared_attr
    def __tablename__(cls):
        return "rel_dialpeer_group"

    dialpeer_id = Column('dialpeer_id', Integer, primary_key=True)
    group_id = Column('group_id', Integer, primary_key=True)


class RelDialpeerTp(object):

    @declared_attr
    def __tablename__(cls):
        return "rel_dialpeer_tp"

    dialpeer_id = Column('dialpeer_id', Integer, primary_key=True)
    tp_id = Column('tp_id', Integer, primary_key=True)


class ColocationIp(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "colocation_ip"

    __table_args__ = {"schema": "routing"}

    id = Column('id', Integer, primary_key=True)


class Colocation(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "colocation"

    __table_args__ = {"schema": "routing"}

    id = Column('id', Integer, primary_key=True)

    CACHE_COLOCATION_PATTERN = "cache::colocation::{id}"


class Host(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "hosts"

    __table_args__ = {"schema": "routing"}

    id = Column('id', Integer, primary_key=True)
    id_colocation = Column('id_colocation', Integer)

    colocation = relationship(
        lambda: Colocation,
        backref="hosts",
        foreign_keys=[id_colocation]
    )


class Codec(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "codecs"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class Protocol(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "protocols"

    __table_args__ = {"schema": "billing"}


class Audit(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "table_audit"

    __table_args__ = {"schema": "public"}

    id = Column('id', Integer, primary_key=True)


class Currency(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "currency"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)



class CurrencyCourse(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "currency_course"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class Balance(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "balance"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)
    currency = relationship(lambda: Currency)

    @hybrid_property
    def name_with_curr(self):
        return "{0} ({1})".format(self.name, self.currency.name_short)

    @name_with_curr.expression
    def name_with_curr_expr(cls):
        return cls.name + " (" + Currency.name_short + ")"


class Client(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "client"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)
    balances = relationship(lambda: Balance, backref="client")
    tps = relationship(lambda: get_entity_for_schema(Tp, "routing"), backref="client")
    ops = relationship(lambda: get_entity_for_schema(Op, "routing"), backref="client")


class RelUserClient(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "rel_user_client"

    __table_args__ = {"schema": "web"}

    user_id = Column('user_id', Integer, primary_key=True)
    client_id = Column('client_id', Integer, primary_key=True)


class AggregatedData(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "aggregated_data"

    __table_args__ = {"schema": "stat"}

    op_setup_time = Column('op_setup_time', DateTime(True), primary_key=True)
    tp_setup_time = Column('tp_setup_time', DateTime(True), primary_key=True)


class McCdr(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "mc_cdr"

    __table_args__ = {"schema": "stat"}

    setup_time = Column('setup_time', DateTime(True), primary_key=True)
    start_time = Column('start_time', DateTime(True), primary_key=True)
    end_time = Column('end_time', DateTime(True), primary_key=True)

    price = Column('price', Numeric(16, 5), nullable=True)
    cost = Column('cost', Numeric(16, 5), nullable=True)
    currency_course = Column('currency_course', Numeric(16, 5), nullable=True)



    class_ = Column('class', Integer)



class CdrClass(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "cdr_classes"

    __table_args__ = {"schema": "stat"}

    id = Column('id', Integer, primary_key=True)


class ProblemType(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "problem_type"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)


class EmailOwner(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "email_owners"

    __table_args__ = {"schema": "api"}

    email_type = Column('email_type', Text, primary_key=True)
    owners_id = Column('owners_id', Text, primary_key=True)


class EmailTemplate(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "email"

    __table_args__ = {"schema": "api"}

    email_type = Column('email_type', Text, primary_key=True)



class EmailSetting(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "email_setting"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)


class WebMail(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "web_mail"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)
    from_ = Column('from', Text)


class CountryCode(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "country_codes"

    __table_args__ = {"schema": "api"}

    dstcode = Column('dstcode', String, primary_key=True)


class PtHistory(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "pt_history"

    id = Column('id', Integer, primary_key=True)
    last_pt_date = Column("last_pt_date", DateTime(True), primary_key=True)

    __table_args__ = {"schema": "history"}


class TpPrice(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "tp_prices"

    id = Column('id', Integer, primary_key=True)
    __table_args__ = {"schema": "billing"}


class DataUpdate(object):

    @declared_attr
    def __tablename__(cls):
        return "data_update"

    data_type = Column('data_type', Integer, primary_key=True)
    data_id = Column('data_id', Integer, primary_key=True)
    added = Column('added', DateTime(True), primary_key=True)
    status = Column('status', Integer, primary_key=True)

create_entity_for_schema(DataUpdate, "routing")


class SessionInfo(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "sessions"

    __table_args__ = {"schema": "api"}

    id = Column('id', Text, primary_key=True)

    def __init__(
        self,
        user_id,
        login,
        password,
        allowed_clients,
        all_comp,
        ip_address="",
        notes="",
        date_log=None
    ):
        if date_log is None:
            date_log = datetime.datetime.utcnow()

        m = hashlib.md5()
        m.update("{0}{1}{2}".format(date_log, login, password))
        sid = m.hexdigest()

        self.id = sid
        self.users_id = user_id
        self.login = login
        self.date_log = date_log
        self.allowed_clients = allowed_clients
        self.all_comp = all_comp
        self.ip_address = ip_address
        self.notes = notes


class CdrRequest(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "cdr_request"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)

    user = relationship(
        lambda: User,
        backref="cdr_request",
    )



class GuardianWarning(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "warning"

    __table_args__ = {"schema": "guardian"}

    id = Column('id', Integer, primary_key=True)


class GuardianWarningReport(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "report_warning"

    __table_args__ = {"schema": "guardian"}

    id = Column('id', Integer, primary_key=True)


class GuardianBlockedDestinations(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "blocked_destinations"

    __table_args__ = {"schema": "guardian"}

    id = Column('id', Integer, primary_key=True)

class GuardianTTRegistry(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "tt_registry"

    __table_args__ = {"schema": "guardian"}

    id = Column('id', Integer, primary_key=True)

class GuardianEmailTemplate(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "email_template"

    __table_args__ = {"schema": "guardian"}

    id = Column('id', Integer, primary_key=True)

class User(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "users"

    __table_args__ = {"schema": "web"}

    id = Column('id', Integer, primary_key=True)

    clients = relationship(
        lambda: Client,
        backref="user",
        secondary=Table("rel_user_client", Reflected.metadata, schema="web", autoload=True)
    )


class Role(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "roles"

    __table_args__ = {"schema": "web"}

    id = Column('id', Integer, primary_key=True)



class RouteType(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "routes"

    __table_args__ = {"schema": "routing"}

    id = Column('id', Integer, primary_key=True)


class TpTarifGroup(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "groups_for_tp_tarif"

    __table_args__ = {"schema": "routing"}

    id = Column('id', Integer, primary_key=True)


class RelGroupTpTarif(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "rel_group_tp_tarif"

    __table_args__ = {"schema": "routing"}

    tarif_id = Column('tarif_id', Integer, primary_key=True)


class PriceDstcode(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "price_dstcode"

    __table_args__ = {"schema": "routing"}

    group_id = Column('group_id', Integer, primary_key=True)
    tarif_id = Column('tarif_id', Integer, primary_key=True)


class NewOpPricesRec(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "new_op_prices_rec"

    __table_args__ = {"schema": "fill"}

    id = Column('id', Integer, primary_key=True)


class NewTpPricesRec(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "new_tp_prices_rec"

    __table_args__ = {"schema": "fill"}

    id = Column('id', Integer, primary_key=True)


class NewPriceStatus(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "new_price_status"

    __table_args__ = {"schema": "fill"}

    id = Column('id', Integer, primary_key=True)


class NewPriceStatusNames(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "new_price_status_names"

    __table_args__ = {"schema": "fill"}

    id = Column('id', Integer, primary_key=True)


class OwnerInfo(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "owner_info"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)


class Owners(Reflected):

    @declared_attr
    def __tablename__(cls):
        return "owners"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)


class LibDisconnectCause(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "lib_disconnect_cause"

    __table_args__ = {"schema": "api"}

    #id = Column('id', Integer, primary_key=True)
    disconnect_cause = Column('disconnect_cause', Integer, primary_key=True)


class LibLocalDisconnectCause(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "lib_local_disconnect_cause"

    __table_args__ = {"schema": "api"}

    disconnect_cause = Column('disconnect_cause', Integer, primary_key=True)


class Configuration(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "configuration"

    __table_args__ = {"schema": "api"}

    web_num = Column('web_num', Integer, primary_key=True)


class PaymentHistory(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "payments_history"

    __table_args__ = {"schema": "history"}

    id = Column('id', Integer, primary_key=True)


class PaymentType(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "payments_type"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class Payment(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "payments"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class PaymentCM(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "payments_callmax"
       
    __table_args__ = {"schema": "billing"}
       
    id = Column('id', Integer, primary_key=True)


class Invoice(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "invoices"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class InvoiceCorrect(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "invoices_correct"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class InvoiceStatus(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "invoice_statuses"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class InvoiceSettings(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "invoice_settings"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class InvoicePeriod(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "invoice_periods"

    __table_args__ = {"schema": "billing"}

    id = Column('id', Integer, primary_key=True)


class FileStorage(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "file_storage"

    __table_args__ = {"schema": "api"}

    f_key = Column('f_key', String, primary_key=True)


class Permission(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "permissions"

    __table_args__ = {"schema": "web"}

    roles_id = Column('roles_id', Integer, primary_key=True)
    properties_id = Column('properties_id', Integer, primary_key=True)


class TimeZone(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "time_zones"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)


class PriceFormat(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "prices_format"

    __table_args__ = {"schema": "api"}

    id = Column('id', Integer, primary_key=True)


class AllowedIp(Reflected):
    @declared_attr
    def __tablename__(cls):
        return "allowed_ips"

    __table_args__ = {"schema": "web"}

    id = Column('id', Integer, primary_key=True)


def init_orm(engine):
    with INIT_ORM_LOCK:
        if getattr(init_orm, "INITIALIZED", False):
            return

        Reflected.prepare(engine)  # reflect DB tables metadata

        S = sessionmaker()
        s = S(bind=engine)

        try:
            # create separate mappings for 'tp' tables in each colocation
            for col in s.query(Colocation).all():
                create_entity_for_schema(
                    OpIp,
                    col.namespace,
                )

                create_entity_for_schema(
                    TpIp,
                    col.namespace,
                    {
                        "protocol_ref": relationship(Protocol),
                    }
                )

                create_entity_for_schema(
                    Tp,
                    col.namespace,
                    {
                        "codecs": relationship(
                            lambda: Codec,
                            secondary=Table(
                                "rel_codecs_tp",
                                Reflected.metadata,
                                schema=col.namespace,
                                autoload=True,
                                autoload_with=engine
                            ),
                        ),

                        "tp_ips": relationship(
                                      get_entity_for_schema(TpIp, col.namespace),
                                      cascade="all, delete, delete-orphan",
                                      backref="tp"
                                  ),

                        "_members_key": "cache::tps::{0}".format(col.namespace),
                        "cache_members_key": classmethod(lambda cls: cls._members_key)
                    },
                )

                create_entity_for_schema(
                    Op,
                    col.namespace,
                    {
                        "op_ips": relationship(
                                      get_entity_for_schema(OpIp, col.namespace),
                                      cascade="all, delete, delete-orphan",
                                      backref="op"
                                  ),

                        "_members_key": "cache::ops::{0}".format(col.namespace),
                        "cache_members_key": classmethod(lambda cls: cls._members_key)
                    },
                )

                create_entity_for_schema(
                    Group,
                    col.namespace,
                )

                create_entity_for_schema(
                    Dialpeer,
                    col.namespace,
                )

                create_entity_for_schema(
                    RelOpGroup,
                    col.namespace,
                    {
                        "op": relationship(
                                get_entity_for_schema(Op, col.namespace),
                                backref="rel_op_group"
                        ),

                        "group": relationship(
                                    get_entity_for_schema(Group, col.namespace),
                                    backref=backref("rel_op_group", cascade="all, delete, delete-orphan")
                        ),
                    }
                )

                create_entity_for_schema(
                    RelTpGroup,
                    col.namespace,
                    {
                        "tp": relationship(
                                get_entity_for_schema(Tp, col.namespace),
                                backref="rel_tp_group"
                        ),

                        "group": relationship(
                                    get_entity_for_schema(Group, col.namespace),
                                    backref=backref("rel_tp_group", cascade="all, delete, delete-orphan")
                        ),
                    }
                )


                create_entity_for_schema(
                    RelDialpeerGroup,
                    col.namespace,
                    {
                        "dialpeer": relationship(
                                get_entity_for_schema(Dialpeer, col.namespace),
                                backref=backref("rel_dialpeer_group", cascade="all, delete, delete-orphan")
                        ),

                        "group": relationship(
                                    get_entity_for_schema(Group, col.namespace),
                                    backref=backref("rel_dialpeer_group", cascade="all, delete, delete-orphan")
                        ),
                    }
                )


                create_entity_for_schema(
                    RelDialpeerTp,
                    col.namespace,
                    {
                        "dialpeer": relationship(
                                get_entity_for_schema(Dialpeer, col.namespace),
                                backref=backref("rel_dialpeer_tp", cascade="all, delete, delete-orphan")
                        ),

                        "tp": relationship(
                                    get_entity_for_schema(Tp, col.namespace),
                                    backref=backref("rel_dialpeer_tp", cascade="all, delete, delete-orphan")
                        ),
                    }
                )

                create_entity_for_schema(
                    DataUpdate,
                    col.namespace
                )

            # update the reflected metadata after adding of new entities
            Reflected.prepare(engine)
            # initialize intermapper relationships
            configure_mappers()

            init_orm.INITIALIZED = True

            # INFO log level is enough to see SQL queries
            import logging
            sa = logging.getLogger("sqlalchemy")
            sa.setLevel(logging.INFO)
        finally:
            s.close()


def get_sessionmaker(host, port, dbname, user, password, existing_engine=None, pooling=False):
    """
        Establish a DB connection and return a sessionmaker instance
    """

    if existing_engine is None:
        engine = create_engine(
            "{dialect}://{user}:{password}@{host}:{port}/{dbname}"
            .format(dialect="postgresql", **locals()),
            poolclass=QueuePool if pooling else NullPool,
            echo=False
        )
    else:
        engine = existing_engine

    init_orm(engine)

    S = sessionmaker(bind=engine)
    return S
