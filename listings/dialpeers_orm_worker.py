#!/usr/bin/env python
# coding: utf-8

import trafaret
import sqlalchemy.exc
from sqlalchemy.orm import outerjoin, join

from billing import errors, status
from billing import lightweightworker, get_config
from billing.model import Colocation, get_entity_for_schema
from billing.model import Tp, RelDialpeerGroup, Dialpeer, RelDialpeerTp

from billing.utils import (
    _dublicate_name_check, limit_offset_sorting,
    _regexp_check, fields_check, sort_by_entry_get, _dataupdate_route_table
)

config = get_config()
service_dp = int(config["db"]["service_dp"])


def dialpeer_add(data, env, stop_monitor, session_info):
    """Creates a new dialpeer or copies an already existing one to given contexts"""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("name"): trafaret.String(),  # a name of dialpeer or new name for dialpeer's copy. Name should be unique.
                trafaret.Key("dstregexp"): trafaret.String(allow_blank=True)
                >> (lambda rg: _regexp_check(rg, "denyregexp")),  # a regular expressions which defines allowed destinations. For example ".*"  - describes all possible destinations,a few expressions should be separated by comma
                trafaret.Key("priority", optional=True, default=0): trafaret.Int(gte=-100, lte=100),  # integer number, dialpeer's priority in range [-100:+100]
                trafaret.Key("enabled", optional=True, default=False): trafaret.Bool(),  # state of dialpeer, if "True" then dialpeer is enabled
                trafaret.Key("stop", optional=True, default=True): trafaret.Bool(),  # boolean option, if "True" then all dialpeers from global dialpeers tree that shorter than current won't participate in the routing.
                trafaret.Key("denyregexp", optional=True, default=''): trafaret.String(allow_blank=True)
                >> (lambda rg: _regexp_check(rg, "denyregexp")),  # regular expression which defines denied destinations for given dialpeer. For example "^3.*$" block calls to destinations which begins with 3, a few expressions should be separated by comma.
                trafaret.Key("ani_regexp", optional=True, default=''): trafaret.String(regex=r"^([^/]+)/([^/]+)$|^$", allow_blank=True)
                >> (lambda rg: _regexp_check(rg.string, "ani_regexp")),  # string variable which  defines  conversion of the caller number with help of regular expression
                trafaret.Key("dnis_regexp", optional=True, default=''): trafaret.String(regex=r"^([^/]+)/([^/]+)$|^$", allow_blank=True)
                >> (lambda rg: _regexp_check(rg.string, "dnis_regexp")),  # string variable which  defines  conversion of the called number with help of regular expressions
                trafaret.Key("delta_price", optional=True, default=0): trafaret.Float(),  # this floating value allows to permit only routes with necessary profitability. If distinction between OP price and TP price in the default currency less than this value then given TP won't participate in routing. This value specifies in default currency.
                trafaret.Key("capacity", optional=True, default=1000): trafaret.Int(gte=0),  # integer value from 0 to 1000, which means amount of concurrent calls and call attempts through the dialpeer
                trafaret.Key("contexts", optional=True): trafaret.List(trafaret.Int(gt=0)),  # contexts id to which dialpeer will be copied
                trafaret.Key("current_context", optional=True): trafaret.Int(gt=0),  # current context id, if this id existed in "contexts" list then relations with groups will be copied in current context too
                trafaret.Key("tp_ids", optional=True): trafaret.List(trafaret.List(trafaret.Int)),  # tps ids linked with the dialpeer, relations with which can be copied to another context(s)
                trafaret.Key("groups_ids", optional=True): trafaret.List(trafaret.List(trafaret.Int)),  # list of groups, to which given dialpeer will be added
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    if checked_data["name"] == "Service_dialpeer_":
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "You can no add dialpeer with such name."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        #_dublicate_name_check(s, get_entity_for_schema(Dialpeer, "routing"), checked_data, 'Dialpeer')
        dialpeer = get_entity_for_schema(Dialpeer, "routing")(
            name=checked_data["name"],
            dstregexp=checked_data["dstregexp"],
            priority=checked_data["priority"],
            enabled=checked_data["enabled"],
            stop=checked_data["stop"],
            denyregexp=checked_data["denyregexp"],
            ani_regexp=checked_data["ani_regexp"],
            dnis_regexp=checked_data["dnis_regexp"],
            delta_price=checked_data["delta_price"],
            capacity=checked_data["capacity"]
        )

        s.add(dialpeer)

        try:
            s.flush()
        except sqlalchemy.exc.DataError as e:
            raise errors.BillingDBError(str(e))

        ctx_list = s.query(Colocation)
        if checked_data.get("contexts"):
            ctx_list = ctx_list.filter(Colocation.id.in_(checked_data["contexts"]))

        for ctx in ctx_list.all():

            _dublicate_name_check(s, get_entity_for_schema(Dialpeer, ctx.namespace), checked_data, 'Dialpeer')

            dialpeer_ctx = get_entity_for_schema(Dialpeer, ctx.namespace)(
                id=dialpeer.id,
                name=checked_data["name"],
                dstregexp=checked_data["dstregexp"],
                priority=checked_data["priority"],
                enabled=checked_data["enabled"],
                stop=checked_data["stop"],
                denyregexp=checked_data["denyregexp"],
                ani_regexp=checked_data["ani_regexp"],
                dnis_regexp=checked_data["dnis_regexp"],
                delta_price=checked_data["delta_price"],
                capacity=checked_data["capacity"]
            )
            s.add(dialpeer_ctx)

            try:
                s.flush()
            except sqlalchemy.exc.DataError as e:
                raise errors.BillingDBError(str(e))

            # add tp-dialpeer relations
            # at the moment this option use from dialpeer copy on web
            if checked_data.get("tp_ids"):
                for tp_data in checked_data["tp_ids"]:
                    rel_dialpeer_tp = get_entity_for_schema(RelDialpeerTp, ctx.namespace)(
                        tp_id=tp_data[0],
                        dialpeer_id=dialpeer.id,
                        priority=tp_data[1],
                        weight=tp_data[2],
                    )

                    s.add(rel_dialpeer_tp)

                try:
                    s.flush()
                except sqlalchemy.exc.IntegrityError as e:
                    raise errors.BillingDBError(str(e))

            # add group-dialpeer relations
            # at the moment this option use from dialpeer copy on web
            # TODO how to do it more correctly?
            if checked_data.get("groups_ids") and ctx.id == checked_data["current_context"]:
                for group_data in checked_data["groups_ids"]:
                    rel_dialpeer_group = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)(
                        group_id=group_data[0],
                        dialpeer_id=dialpeer.id,
                    )

                    s.add(rel_dialpeer_group)

                try:
                    s.flush()
                except sqlalchemy.exc.IntegrityError as e:
                    raise errors.BillingDBError(str(e))

            #data_update
            _dataupdate_route_table(s, ctx)

        return {"id": dialpeer.id}


@lightweightworker.invalidate_responses(["tps_dialpeers", "groups_dialpeers"])
def dialpeer_set(data, env, stop_monitor, session_info):
    """Updates general settings of dialpeer with given id within given context(s) and in schema routing.
    This function disallows to edit service dialpeers."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("id"): trafaret.Int(gt=0),  # id of the dialpeer in database
                trafaret.Key("name", optional=True): trafaret.String(),  # dialpeer name
                trafaret.Key("dstregexp", optional=True): trafaret.String(allow_blank=True)
                >> (lambda rg: _regexp_check(rg, "denyregexp")),  # regexp=REGEXP_VALIDATION), a regular expressions which defines allowed destinations. For example ".*"  - describes all possible destinations,a few expressions should be separated by comma
                trafaret.Key("priority", optional=True): trafaret.Int(gte=-100, lte=100),  # dialpeer's priority in range from -100 to +100
                trafaret.Key("enabled", optional=True): trafaret.Bool(),  # dialpeer's state, if True - dialpeer is enabled
                trafaret.Key("stop", optional=True): trafaret.Bool(),  # boolean option, if "True" then all dialpeers from global dialpeers tree that shorter than current won't participate in the routing
                trafaret.Key("ani_regexp", optional=True, default=''): trafaret.String(regex=r"^([^/]+)/([^/]+)$|^$", allow_blank=True)
                >> (lambda rg: _regexp_check(rg.string, "ani_regexp")),  # string variable which  defines  conversion of the caller number with help of regular expression
                trafaret.Key("dnis_regexp", optional=True, default=''): trafaret.String(regex=r"^([^/]+)/([^/]+)$|^$", allow_blank=True)
                >> (lambda rg: _regexp_check(rg.string, "dnis_regexp")),  # string variable which  defines  conversion of the called number with help of regular expressions
                trafaret.Key("denyregexp", optional=True, default=''): trafaret.String(allow_blank=True)
                >> (lambda rg: _regexp_check(rg, "denyregexp")),  # regular expression which defines denied destinations for given dialpeer. For example "^3.*$" block calls to destinations which begins with 3, a few expressions should be separated by comma.
                trafaret.Key("delta_price", optional=True): trafaret.Float(),  # this floating value allows to permit only routes with necessary profitability. If distinction between OP price and TP price n the default currency less than this value then given TP won't participate in routing. This value specifies in default currency.
                trafaret.Key("capacity", optional=True): trafaret.Int(gte=0),  # integer value from 0 to 1000 which means amount of concurrent calls and call attempts through the dialpeer
                trafaret.Key("contexts"): trafaret.List(trafaret.Int(gt=0)),  # contexts ids where dialpeer will be changed
            }
        ).ignore_extra('*')

        checked_data = data_validation.check(data)

    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    if checked_data["id"] == service_dp:
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "You can not edit service dialpeer."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        dialpeer = s.query(get_entity_for_schema(Dialpeer, "routing"))\
            .filter_by(id=checked_data["id"])\
            .first()
        if dialpeer is None:
            raise errors.BillingDBError("There is no dialpeer with id {0} in DB".format(checked_data["id"]))

        for f in checked_data.keys():
            setattr(dialpeer, f, checked_data.get(f))
        s.flush()

        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation) \
                   .filter_by(id=ctx_id) \
                   .first()
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            _dublicate_name_check(s, get_entity_for_schema(Dialpeer, ctx.namespace), checked_data, 'Dialpeer')

            dialpeer_ctx = s.query(get_entity_for_schema(Dialpeer, ctx.namespace))\
                .filter_by(id=checked_data["id"]) \
                .first()
            if dialpeer_ctx is None:
                raise errors.BillingDBError("There is no dialpeer with id {0} in DB".format(checked_data["id"]))
            for f in checked_data.keys():
                setattr(dialpeer_ctx, f, checked_data.get(f))

            #data_update
            _dataupdate_route_table(s, ctx)

        return {"id": dialpeer.id}


def dialpeer_copy(data, env, stop_monitor, session_info):
    """Deprecated. Currently not in use."""
    try:
        context_validation = trafaret.Dict(
            {
                trafaret.Key("id"): trafaret.Int(gt=0),
                trafaret.Key("name"): trafaret.String(),
                trafaret.Key("contexts"): trafaret.List(trafaret.Int(gt=0)),
            }
        ).ignore_extra('*')
        checked_data = context_validation.check(data)

    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        DialpeerRtg = get_entity_for_schema(Dialpeer, "routing")

        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).filter_by(id=ctx_id).first()
            if ctx is None:
                raise errors.BillingDBError(
                    "There is no context with id {0} in DB".format(ctx_id))

            DialpeerCtx = get_entity_for_schema(Dialpeer, ctx.namespace)
            RelDialpeerTpCtx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)

            dialpeer_old = s.query(DialpeerCtx)\
                            .filter_by(id=checked_data["id"])\
                            .first()
            if dialpeer_old is None:
                raise errors.BillingDBError(
                    "There is no dialpeer with id {0} in DB".format(checked_data["id"]))

            _dublicate_name_check(s, DialpeerRtg, checked_data, 'Dialpeer')

            dialpeer_new = DialpeerRtg(
                name=checked_data["name"],
                dstregexp=dialpeer_old.dstregexp,
                priority=dialpeer_old.priority,
                enabled=dialpeer_old.enabled,
                stop=dialpeer_old.stop,
                denyregexp=dialpeer_old.denyregexp,
                ani_regexp=dialpeer_old.ani_regexp,
                dnis_regexp=dialpeer_old.dnis_regexp,
                delta_price=dialpeer_old.delta_price,
                capacity=dialpeer_old.capacity
            )

            s.add(dialpeer_new)
            try:
                s.flush()
            except sqlalchemy.exc.IntegrityError:
                raise errors.BillingError(
                    status.WRONG_INPUT,
                    "Dialpeer with name `{0}` has already existed.".format(checked_data["name"])
                )

            except sqlalchemy.exc.DataError as e:
                raise errors.BillingDBError(str(e))

            dialpeer_new_ctx = DialpeerCtx(
                id=dialpeer_new.id,
                name=checked_data["name"],
                dstregexp=dialpeer_old.dstregexp,
                priority=dialpeer_old.priority,
                enabled=dialpeer_old.enabled,
                stop=dialpeer_old.stop,
                denyregexp=dialpeer_old.denyregexp,
                ani_regexp=dialpeer_old.ani_regexp,
                dnis_regexp=dialpeer_old.dnis_regexp,
                delta_price=dialpeer_old.delta_price,
                capacity=dialpeer_old.capacity,
            )
            s.add(dialpeer_new_ctx)
            try:
                s.flush()
            except sqlalchemy.exc.IntegrityError:
                raise errors.BillingError(
                    status.WRONG_INPUT,
                    "Dialpeer with name `{0}` has already existed.".format(checked_data["name"])
                )
            except sqlalchemy.exc.DataError as e:
                raise errors.BillingDBError(str(e))

            rel_dialpeer_tp_old = s.query(RelDialpeerTpCtx)\
                .filter_by(dialpeer_id=dialpeer_old.id)\
                .all()

            for rdt in rel_dialpeer_tp_old:
                rel_dialpeer_tp = RelDialpeerTpCtx(
                    tp_id=rdt.tp_id,
                    dialpeer_id=dialpeer_new.id,
                    priority=rdt.priority,
                    weight=rdt.weight,
                )

                s.add(rel_dialpeer_tp)

            _dataupdate_route_table(s, ctx)

        return {"id": dialpeer_new_ctx.id}


def dialpeers_get(data, env, stop_monitor, session_info):
    """If dialpeer's id don't set explicitly in query function returns parameters of dialpeers within given context(s) which satisfy
    'name' filter.
    If dialpeer id specified - function returns settings of chosen dialpeer within given context(s)."""
    default_fields = [
        'id',  # Int
        'name',  # String
        'name_context',  # String
        'dstregexp',  # String
        'priority',  # Int
        'enabled',  # Boolean
        'stop',  # Boolean
        'denyregexp',  # String
        'ani_regexp',  # String
        'dnis_regexp',  # String
        'capacity',  # String
        'delta_price',  # numeric(16,5)
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("id", optional=True): trafaret.Int(gt=0),  # dialpeer id
                trafaret.Key("name", optional=True): trafaret.String(),  # first symbols of dialpeer name
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # required context ids
                trafaret.Key("fields", default=default_fields, optional=True): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field name on which result will be sorted
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # rows quantity per one request
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0),  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        results = []
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).get(ctx_id)
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            DialpeerCtx = get_entity_for_schema(Dialpeer, ctx.namespace)

            get_field = {
                "name_context": DialpeerCtx.name_with_context
            }

            dialpeers = s.query(
                *[get_field[f] if f in get_field else getattr(DialpeerCtx, f) for f in checked_data["fields"]]
            )

            # we do not show service dialpeer on web
            dialpeers = dialpeers.filter(DialpeerCtx.id != service_dp)

            if "id" in checked_data:
                dialpeers = dialpeers.filter(DialpeerCtx.id == checked_data["id"])
            if "name" in checked_data:
                dialpeers = dialpeers.filter(DialpeerCtx.name.ilike(checked_data["name"] + "%"))

            dialpeers = limit_offset_sorting(dialpeers, DialpeerCtx, checked_data, get_field)
            results.extend(list(d) for d in dialpeers)

        return {"fields": checked_data["fields"], "data": results}


@lightweightworker.invalidate_responses(["tps_dialpeers"])
def tps_to_dialpeers_add(data, env, stop_monitor, session_info):
    """Creates relations between chosen dialpeer and TP(s) or relations between chosen TP and dialpeer(s) within given context(s)."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of termination points ids. Points should be visible.
                trafaret.Key("dialpeers_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of dialpeers ids
                trafaret.Key("priority", optional=True, default=0): trafaret.Int(gte=-100, lte=100),  # relations priority, all relations have the same priority
                trafaret.Key("weight", optional=True, default=0): trafaret.Int(gte=0, lte=100),  # relations weight, all relations will have the same weight, in case when dialpeers adding to TP weight equal the default value 0.
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # required contexts ids
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    # simple routing check
    if service_dp in checked_data["dialpeers_ids"]:
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "Relation was created in simple mode. You can't change it in advanced mode."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).filter_by(id=ctx_id).first()
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            #TODO add lock by company check
            for tp_id in checked_data["tp_ids"]:

                #check for invisible on tp
                tp_check = s.query(get_entity_for_schema(Tp, ctx.namespace))\
                            .filter_by(id=tp_id)\
                            .first()
                if tp_check is None:
                    raise errors.BillingDBError(
                        "There is no TP with id {0} in DB".format(tp_id))

                if tp_check.invisible:
                    raise errors.BillingDBError(
                        "You can not add invisible point to dialpeer")

                for dialpeer_id in checked_data["dialpeers_ids"]:
                    rel_dialpeer_tp = get_entity_for_schema(RelDialpeerTp, ctx.namespace)(
                        tp_id=tp_id,
                        dialpeer_id=dialpeer_id,
                        priority=checked_data["priority"],
                        weight=checked_data["weight"],
                    )

                    s.add(rel_dialpeer_tp)

                    _dataupdate_route_table(s, ctx)

        return {}


@lightweightworker.invalidate_responses(["tps_dialpeers"])
def dialpeer_tp_set(data, env, stop_monitor, session_info):
    """Changes priority and weight of given TP<-->Dialpeer relation within chosen context(s). Function disallows to change service dialpeer relations."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_id"): trafaret.Int(gt=0),  # termination point id in database
                trafaret.Key("dialpeer_id"): trafaret.Int(gt=0),  # id of dialpeer in database
                trafaret.Key("priority", optional=True, default=0): trafaret.Int(gte=-100, lte=100),  # relation priority in range from -100 to 100 inclusive
                trafaret.Key("weight", optional=True, default=0): trafaret.Int(gte=0, lte=100),  # weight of the relation in range from 0 to 100 inclusive
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # contexts ids where need change given relation
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    if checked_data["dialpeer_id"] == service_dp:
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "You can not edit relation with service dialpeer."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation)\
                   .filter_by(id=ctx_id)\
                   .first()
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            #TODO add lock by company check
            rel_dialpeer_tp = s.query(get_entity_for_schema(RelDialpeerTp, ctx.namespace))\
                               .filter_by(tp_id=checked_data["tp_id"])\
                               .filter_by(dialpeer_id=checked_data["dialpeer_id"])\
                               .first()
            if rel_dialpeer_tp is None:
                raise errors.BillingDBError(
                    "There is no relation between TP {0} and dialpeer {1} in DB".format(
                        checked_data["tp_id"],
                        checked_data["dialpeer_id"]
                    )
                )

            for field in ('priority', 'weight',):
                setattr(rel_dialpeer_tp, field, checked_data.get(field))

            _dataupdate_route_table(s, ctx)

        return {}


@lightweightworker.invalidate_responses(["tps_dialpeers"])
def tps_from_dialpeers_del(data, env, stop_monitor, session_info):
    """Removes relations between chosen dialpeer and TP(s) or between chosen TP and dialpeer(s) within given context(s).
    Function disallows to delete relations with service dialpeer."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_ids"): trafaret.List(trafaret.Int(gt=0)),  # termination points ids list
                trafaret.Key("dialpeers_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of of dialpeer ids which shouldn't contain service dialpeer's id
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # required contexts ids
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    if service_dp in checked_data["dialpeers_ids"]:
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "You can not delete relation with service dialpeer."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).filter_by(id=ctx_id).first()
            if ctx is None:
                raise errors.BillingDBError(
                    "There is no context with id {0} in DB".format(ctx_id)
                )

            #TODO add lock by company_check
            for tp_id in checked_data["tp_ids"]:
                for dialpeer_id in checked_data["dialpeers_ids"]:
                    rel_tp_dialpeer = s.query(get_entity_for_schema(RelDialpeerTp, ctx.namespace))\
                                       .filter_by(tp_id=tp_id)\
                                       .filter_by(dialpeer_id=dialpeer_id)\
                                       .first()
                    if rel_tp_dialpeer is None:
                        raise errors.BillingDBError(
                            "There is no relation between TP {0} and dialpeer {1} in DB".format(tp_id, dialpeer_id)
                        )

                    s.delete(rel_tp_dialpeer)
                    _dataupdate_route_table(s, ctx)

        return {}


@lightweightworker.invalidate_responses(["groups_dialpeers"])
def groups_to_dialpeers_add(data, env, stop_monitor, session_info):
    """Creates relations between chosen dialpeer and groups or chosen group and dialpeers.
    This function disallows to add relation between groups and service dialpeer."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("groups_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of groups ids
                trafaret.Key("dialpeers_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of dialpeers ids which shouldn't contain ids of service dialpeers.
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # contexts ids where need add relations will be added
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    if service_dp in checked_data["dialpeers_ids"]:
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "You can not add group to service dialpeer."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).filter_by(id=ctx_id).first()
            if ctx is None:
                raise errors.BillingDBError(
                    "There is no context with id {0} in DB".format(ctx_id)
                )

            # simple routing check
            RDG_ctx = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)
            groups_check = s.query(RDG_ctx)\
                            .filter(RDG_ctx.group_id.in_(checked_data["groups_ids"]))\
                            .filter(RDG_ctx.dialpeer_id == service_dp)\
                            .all()

            if groups_check:
                raise errors.BillingError(
                    status.NOT_ACCEPTABLE,
                    "Relation was created in simple mode. You can't change it in advanced mode."
                )

            for group_id in checked_data["groups_ids"]:
                for dialpeer_id in checked_data["dialpeers_ids"]:
                    rel_dialpeer_group = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)(
                        group_id=group_id,
                        dialpeer_id=dialpeer_id,
                    )

                    s.add(rel_dialpeer_group)

            _dataupdate_route_table(s, ctx)

        return {}


@lightweightworker.invalidate_responses(["groups_dialpeers"])
def dialpeers_from_groups_del(data, env, stop_monitor, session_info):
    """Removes relations between chosen dialpeers and groups. Function doesn't allow to remove relations between
    groups and service dialpeers and raises error if user tries to do it.."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("dialpeers_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of dialpeer ids
                trafaret.Key("groups_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of group ids
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # contexts ids where need remove specified relations
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    if service_dp in checked_data["dialpeers_ids"]:
        raise errors.BillingError(
            status.NOT_ACCEPTABLE,
            "You can not delete group from service dialpeer."
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).filter_by(id=ctx_id).first()
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            # simple routing check
            RDG_ctx = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)
            groups_check = s.query(RDG_ctx)\
                            .filter(RDG_ctx.group_id.in_(checked_data["groups_ids"]))\
                            .filter(RDG_ctx.dialpeer_id == service_dp)\
                            .all()

            if groups_check:
                raise errors.BillingError(
                    status.NOT_ACCEPTABLE,
                    "Relation was created in simple mode. You can't change it in advanced mode."
                )

            for dialpeer_id in checked_data["dialpeers_ids"]:
                for group_id in checked_data["groups_ids"]:
                    rel_dialpeer_group = s.query(get_entity_for_schema(RelDialpeerGroup, ctx.namespace))\
                                          .filter_by(dialpeer_id=dialpeer_id)\
                                          .filter_by(group_id=group_id)\
                                          .first()
                    if rel_dialpeer_group is None:
                        raise errors.BillingDBError(
                            "There is no relation between dialpeer {0} and group {1} in DB".format(dialpeer_id, group_id)
                        )

                    s.delete(rel_dialpeer_group)

            _dataupdate_route_table(s, ctx)

        return {}


def dialpeers_tp_get(data, env, stop_monitor, session_info):
    """Returns information about dialpeers that connected with chosen termination point and chosen group.
    Can show such  dialpeers settings as 'dstregexp', 'denyregexp', 'ani_regexp', dialpeer status and so on."""
    default_fields = [
        'id',  # Int
        'name',  # String
        'dstregexp',  # String
        'priority',  # Int
        'enabled',  # Boolean
        'stop',  # Boolean
        'denyregexp',  # String
        'ani_regexp',  # String
        'dnis_regexp',  # String
        'delta_price',  # numeric(16,5)
    ]

    try:
        sort_by_entry = trafaret.Dict(
            dict((trafaret.Key(field, optional=True), trafaret.String(allow_blank=True, regex="^(asc)|(desc)|()$")) for field in default_fields)
        )

        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_id"): trafaret.Int(),  # termination point id
                trafaret.Key("group_id"): trafaret.Int(),  # group_id
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # required contexts ids
                trafaret.Key("fields", default=default_fields, optional=True): trafaret.List(trafaret.String()),
                trafaret.Key("sort_by", default=[], optional=True): trafaret.List(sort_by_entry),  # column name on which result sorts
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # rows quantity per one request
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0),  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        if checked_data["contexts"]:
            contexts = s.query(Colocation) \
                        .filter(Colocation.id.in_(checked_data["contexts"])) \
                        .all()

        results = []
        for ctx in contexts:
            RelDialpeerTpCtx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)
            RelDialpeerGroupCtx = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)
            DialpeerCtx = get_entity_for_schema(Dialpeer, ctx.namespace)

            fields_map = {
                'id': DialpeerCtx.id,
                'name': DialpeerCtx.name,
                'dstregexp': DialpeerCtx.dstregexp,
                'priority': DialpeerCtx.priority,
                'enabled': DialpeerCtx.enabled,
                'stop': DialpeerCtx.stop,
                'denyregexp': DialpeerCtx.denyregexp,
                'ani_regexp': DialpeerCtx.ani_regexp,
                'dnis_regexp': DialpeerCtx.dnis_regexp,
                'delta_price': DialpeerCtx.delta_price
            }

            dialpeers = s.query(
                *[fields_map[f] for f in checked_data["fields"] if f in default_fields]
            ).select_from(join(RelDialpeerGroupCtx, DialpeerCtx).join(RelDialpeerTpCtx))

            dialpeers = dialpeers.filter(RelDialpeerTpCtx.tp_id == checked_data["tp_id"]) \
                                 .filter(RelDialpeerGroupCtx.group_id == checked_data["group_id"])

            results.extend(list(r) for r in dialpeers)

        return {"fields": checked_data["fields"], "data": results}


@lightweightworker.cache_response("tps_dialpeers", 3600)
def tp_free_dialpeers_get(data, env, stop_monitor, session_info):
    """Returns list of dialpeers that not connected with chosen TP"""
    default_fields = [
        "dialpeer_id",  # Int
        "dialpeer_name",  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("dialpeer_name", optional=True): trafaret.String(allow_blank=True),  # Filter by dialpeer's name
                trafaret.Key("tp_id", optional=True): trafaret.Int(),  # termination point id
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # required contexts ids
                trafaret.Key("fields", default=default_fields, optional=True): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # requested fields, by defaults names mentioned in default fields
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which list will be sorted
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # rows quantity per one request
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0),  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        results = []
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).get(ctx_id)
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            DialpeerCtx = get_entity_for_schema(Dialpeer, ctx.namespace)
            RelDialpeerTpCtx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)

            get_field = {
                "dialpeer_id": DialpeerCtx.id,
                "dialpeer_name": DialpeerCtx.name_with_context
            }

            dialpeers = s.query(*[get_field[f] for f in checked_data["fields"]])

            # we do not show service dialpeer on web
            dialpeers = dialpeers.filter(DialpeerCtx.id != service_dp)

            if checked_data.get("tp_id"):
                bound_dialpeers = s.query(RelDialpeerTpCtx.dialpeer_id) \
                                   .filter(RelDialpeerTpCtx.tp_id == checked_data["tp_id"]) \
                                   .distinct()
                dialpeers = dialpeers.filter(~DialpeerCtx.id.in_(bound_dialpeers))

            if checked_data.get("dialpeer_name"):
                dialpeers = dialpeers.filter(DialpeerCtx.name.ilike(checked_data["dialpeer_name"] + "%"))

            dialpeers = limit_offset_sorting(dialpeers, DialpeerCtx, checked_data, get_field)
            results.extend(list(d) for d in dialpeers)

        return {"fields": checked_data["fields"], "data": results}


def dialpeers_tps_get(data, env, stop_monitor, session_info):
    """Returns list of dialpeers binded with chosen TP or TPS binded with chosen dialpeer and parameters (priority and
    weight) of their relation. Function can filter output by TP's name or dialpeer's name. Filter by TP name should
    be used in conjunction with dialpeer's id, filter by dialpeer name - in conjunction with TP's id."""
    default_fields = [
        "dialpeer_id",  # Int
        "tp_id",  # Int
        "priority",  # Int
        "tp_name",  # String
        "dialpeer_name",  # String
        "weight",  # Int
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_id", optional=True): trafaret.Int(),  # termination point id
                trafaret.Key("dialpeer_id", optional=True): trafaret.Int(),  # dialpeer id
                trafaret.Key("tp_name", optional=True): trafaret.String(allow_blank=True),  # filter by first symbols of TP name
                trafaret.Key("dialpeer_name", optional=True): trafaret.String(allow_blank=True),  # filter by first symbols of dialpeer name
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # required contexts ids
                trafaret.Key("fields", default=default_fields, optional=True): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # requested fields, by defaults names mentioned in "default fields"
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which list will be sorted
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # rows quantity per one request
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0),  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        results = []
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).get(ctx_id)
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            TpCtx = get_entity_for_schema(Tp, ctx.namespace)
            DialpeerCtx = get_entity_for_schema(Dialpeer, ctx.namespace)
            RelDialpeerTpCtx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)

            get_field = {
                "weight": RelDialpeerTpCtx.weight,
                "priority": RelDialpeerTpCtx.priority,
                "tp_id": TpCtx.id,
                "tp_name": TpCtx.name,
                "dialpeer_id": DialpeerCtx.id,
                "dialpeer_name": DialpeerCtx.name_with_context
            }

            query = s.query(*[get_field[f] for f in checked_data["fields"]]) \
                     .select_from(outerjoin(DialpeerCtx, RelDialpeerTpCtx).outerjoin(TpCtx)) \
                     .filter(TpCtx.invisible == False)

            # we do not show service dialpeer on web
            query = query.filter(RelDialpeerTpCtx.dialpeer_id != service_dp)

            if session_info.get("companies", []):
                query = query.filter(TpCtx.client_id.in_(session_info["companies"]))
            if checked_data.get("dialpeer_name"):
                query = query.filter(DialpeerCtx.name.ilike(checked_data["dialpeer_name"] + "%"))
            if checked_data.get("dialpeer_id"):
                query = query.filter(RelDialpeerTpCtx.dialpeer_id == checked_data["dialpeer_id"])
            if checked_data.get("tp_id"):
                query = query.filter(TpCtx.id == checked_data["tp_id"])
            if checked_data.get("tp_name"):
                query = query.filter(TpCtx.name.ilike(checked_data["tp_name"] + "%"))

            query = limit_offset_sorting(query, RelDialpeerTpCtx, checked_data, get_field)
            results.extend(list(d) for d in query)

        return {"fields": checked_data["fields"], "data": results}


def dialpeer_tps_op_get(data, env, stop_monitor, session_info):
    """Returns termination points connected with chosen OP via chosen group and dialpeer in the given context(s)"""
    default_fields = [
        'dialpeer_id',  # Int
        'tp_id',  # Int
        'priority',  # Int
        'tp_name',  # String
        'weight',  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("op_id"): trafaret.Int(),  # chosen OP id
                trafaret.Key("dialpeer_id"): trafaret.Int(),  # chosen dialpeer id
                trafaret.Key("group_id"): trafaret.Int(),  # chosen group id
                trafaret.Key("contexts"): trafaret.List(trafaret.Int()),  # required contexts ids
                trafaret.Key("fields", default=default_fields, optional=True): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which list of groups and dialpeers will be sorted
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # rows quantity per one request
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0),  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).get(ctx_id)
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            fields_map = {
                'dialpeer_id': 'rdt.dialpeer_id',
                'tp_id': 'tp_id',
                'priority': 'rdt.priority',
                'tp_name': 'tp.name',
                'weight': 'rdt.weight'
            }

            query = 'SELECT DISTINCT ' + ','.join([fields_map[f] for f in checked_data['fields']])
            query += """ FROM {0}.rel_dialpeer_tp rdt
               INNER JOIN {0}.tp ON {0}.tp.id=rdt.tp_id
               INNER JOIN {0}.rel_dialpeer_group rdg
                   ON rdt.dialpeer_id = rdg.dialpeer_id
               WHERE rdt.dialpeer_id=:dialpeer_id AND tp_id IN
                   (SELECT distinct rtg.tp_id FROM {0}.rel_op_group rog
                   INNER JOIN {0}.rel_tp_group rtg on rog.group_id=rtg.group_id
                   WHERE rog.op_id=:op_id
                   AND rtg.group_id=:group_id AND rtg.group_id IN
                       (SELECT group_id FROM {0}.rel_dialpeer_group WHERE dialpeer_id=:dialpeer_id))
            """

            if session_info.get("companies", []):
                company_list = [str(i) for i in session_info.get("companies")]
                query += " AND {0}.tp.client_id IN (" + ', '.join(company_list) + ')'

            results = s.execute(query.format(ctx.namespace), checked_data)

            results = [list(i) for i in results.fetchall()]

        return {"fields": checked_data["fields"], "data": results}


@lightweightworker.cache_response("tps_dialpeers", 3600)
def tps_free_dialpeer_get(data, env, stop_monitor, session_info):
    """Returns list of termination points that don't connected with chosen dialpeer"""
    default_fields = [
        'tp_id',  # Int
        'tp_name',  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_name", optional=True): trafaret.String(allow_blank=True),  # termination point name
                trafaret.Key("tp_id", optional=True): trafaret.Int(),  # id of the termination point
                trafaret.Key("dialpeer_id", optional=True): trafaret.Int(),  # dialpeer id
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # ids of required contexts
                trafaret.Key("fields", default=default_fields, optional=True): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # requested fields, by defaults fields mentioned in default fields
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which list will be sorted
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # rows quantity per one request
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0),  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra('*')
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        results = []
        for ctx_id in checked_data["contexts"]:
            ctx = s.query(Colocation).get(ctx_id)
            if ctx is None:
                raise errors.BillingDBError("There is no context with id {0} in DB".format(ctx_id))

            RelDialpeerTpCtx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)
            TpCtx = get_entity_for_schema(Tp, ctx.namespace)

            get_field = {
                "tp_id": TpCtx.id,
                "tp_name": TpCtx.name
            }

            rel_tp_dialpeer = s.query(RelDialpeerTpCtx.tp_id).distinct()
            if checked_data.get("dialpeer_id"):
                rel_tp_dialpeer = rel_tp_dialpeer.filter_by(dialpeer_id=checked_data["dialpeer_id"])

            tps = s.query(*[get_field[f] for f in checked_data["fields"]]) \
                   .filter(~TpCtx.id.in_(rel_tp_dialpeer)) \
                   .filter(TpCtx.invisible == False)

            if "tp_name" in checked_data and checked_data["tp_name"]:
                tps = tps.filter(TpCtx.name.ilike(checked_data["tp_name"] + "%"))
            if session_info.get("companies", []):
                tps = tps.filter(TpCtx.client_id.in_(session_info["companies"]))

            tps = limit_offset_sorting(tps, TpCtx, checked_data, get_field)
            results.extend(list(t) for t in tps)

        return {"fields": checked_data["fields"], "data": results}


__API__ = {
    "dialpeer_add": dialpeer_add,
    "dialpeer_set": dialpeer_set,
    "dialpeer_copy": dialpeer_copy,
    "dialpeers_get": dialpeers_get,
    "tps_to_dialpeers_add": tps_to_dialpeers_add,
    "dialpeer_tp_set": dialpeer_tp_set,
    "tps_from_dialpeers_del": tps_from_dialpeers_del,
    "groups_to_dialpeers_add": groups_to_dialpeers_add,
    "dialpeers_from_groups_del": dialpeers_from_groups_del,
    "dialpeer_tps_op_get": dialpeer_tps_op_get,
    "tp_free_dialpeers_get": tp_free_dialpeers_get,
    "dialpeers_tps_get": dialpeers_tps_get,
    "dialpeers_tp_get": dialpeers_tp_get,
    "tps_free_dialpeer_get": tps_free_dialpeer_get
}
