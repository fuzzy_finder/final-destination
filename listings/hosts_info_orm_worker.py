#!/usr/bin/python

import datetime

import concurrent.futures
from pysnmp.entity.rfc3413.oneliner import cmdgen

import trafaret

from billing import status, errors, get_config
from billing.model import Host
from billing.utils import fields_check

config = get_config()


def __get_snmp_data(host, *snmp_var_names):
    snmp_version = int(config[host].get('snmp_version', 3))
    snmp_port = int(config['snmp']['snmp_port'])
    if snmp_version == 2:
        # v2 settings
        snmp_sec_name = config['snmp']['snmp_sec_name']
        snmp_community = config['snmp']['snmp_community']
        auth_data = cmdgen.CommunityData(
            snmp_sec_name,
            snmp_community
        )
    elif snmp_version == 3:
        # v3 settings
        snmp_security_name = config['snmp']['security_name']
        snmp_auth_key = config['snmp']['auth_key']
        snmp_auth_protocol = cmdgen.usmHMACMD5AuthProtocol
        auth_data = cmdgen.UsmUserData(
            snmp_security_name,
            authKey=snmp_auth_key,
            authProtocol=snmp_auth_protocol
        )
    else:
        raise errors.BillingError(
            status.INTERNAL_SERVER_ERROR,
            "Wrong snmp version - `{0}`".format(snmp_version)
        )

    errorIndication, errorStatus, errorIndex, varBinds = cmdgen.CommandGenerator().getCmd(
        auth_data,
        cmdgen.UdpTransportTarget((host, snmp_port)),
        *snmp_var_names
    )
    if errorIndication:
        raise errors.BillingError(
            status.INTERNAL_SERVER_ERROR,
            errorIndication
        )

    return varBinds


def _get_system_info(host):
    snmp_data = __get_snmp_data(
        host,
        (1, 3, 6, 1, 2, 1, 1, 3, 0),  # uptime - uptime (in hundredths of a second)
        (1, 3, 6, 1, 4, 1, 2021, 100, 4, 0),  # current date/time
    )
    system_info = [str(i[1]) for i in snmp_data]
    system_info_name = ['Uptime', 'Date']
    system_info[0] = str(datetime.timedelta(seconds=int(system_info[0]) / 100))
    system_info = "\n".join(["%s: %s" % (i, j) for i, j in zip(system_info_name, system_info)])
    return system_info


def _get_cpu_info(host):
    snmp_data = __get_snmp_data(
        host,
        (1, 3, 6, 1, 4, 1, 2021, 10, 1, 3, 1),  # 1-minute system load on the target host - Load Average
        (1, 3, 6, 1, 4, 1, 2021, 11, 9, 0),   # user CPU time
        (1, 3, 6, 1, 4, 1, 2021, 11, 10, 0),  # system CPU time
        (1, 3, 6, 1, 4, 1, 2021, 11, 11, 0)  # idle CPU time
    )
    cpu_info = [str(i[1]) for i in snmp_data]
    cpu_info_name = ['Load average', 'User CPU time(%)', 'System CPU time(%)', 'Idle CPU time(%)']
    cpu_info = "\n".join(["%s: %s" % (i, j) for i, j in zip(cpu_info_name, cpu_info)])
    print cpu_info
    return cpu_info


def _get_memory_info(host):
    snmp_data = __get_snmp_data(
        host,
        (1, 3, 6, 1, 4, 1, 2021, 4, 5, 0),  # Total RAM in machine, kB
        (1, 3, 6, 1, 4, 1, 2021, 4, 11, 0)  # Total RAM Free, kB
    )
    memory_info = [str(i[1]) for i in snmp_data]
    memory_info_name = ['Total RAM(kB)',  'Total RAM available(kB)']
    memory_info = "\n".join(["%s: %s" % (i, j) for i, j in zip(memory_info_name, memory_info)])
    return memory_info


def _get_disk_info(host):
    snmp_data = __get_snmp_data(
        host,
        (1, 3, 6, 1, 4, 1, 2021, 9, 1, 3, 1),  # Path of the device for the partition
        (1, 3, 6, 1, 4, 1, 2021, 9, 1, 6, 1),  # Total size of the disk/partion (kBytes)
        (1, 3, 6, 1, 4, 1, 2021, 9, 1, 7, 1),  # Available space on the disk
        (1, 3, 6, 1, 4, 1, 2021, 9, 1, 8, 1),  # Used space on the disk, kB
        (1, 3, 6, 1, 4, 1, 2021, 9, 1, 9, 1)   # Used space on the disk, %
    )
    disk_info = [str(i[1]) for i in snmp_data]
    disk_info_name = ['Device', 'Total size(kB)', 'Available space(kB)', 'Used space(kB)', 'Space used on disk(%)']
    disk_info = "\n".join(["%s: %s" % (i, j) for i, j in zip(disk_info_name, disk_info)])
    return disk_info


def _get_ethernet_info(host):
    snmp_data = __get_snmp_data(
        host,
        (1, 3, 6, 1, 2, 1, 4, 21, 1, 7, 0, 0, 0, 0)  # GW_IP
    )
    GW_IP = snmp_data[0][1].prettyPrint()

    # FIXME we get interface data we use .nextCmd not .getCmd
    snmp_version = int(config[host].get('snmp_version', 3))
    snmp_port = int(config['snmp']['snmp_port'])
    if snmp_version == 2:
        # v2 settings
        snmp_sec_name = config['snmp']['snmp_sec_name']
        snmp_community = config['snmp']['snmp_community']
        auth_data = cmdgen.CommunityData(
            snmp_sec_name,
            snmp_community
        )
    elif snmp_version == 3:
        # v3 settings
        snmp_security_name = config['snmp']['security_name']
        snmp_auth_key = config['snmp']['auth_key']
        snmp_auth_protocol = cmdgen.usmHMACMD5AuthProtocol
        auth_data = cmdgen.UsmUserData(
            snmp_security_name,
            authKey=snmp_auth_key,
            authProtocol=snmp_auth_protocol
        )
    else:
        raise errors.BillingError(
            status.INTERNAL_SERVER_ERROR,
            "Wrong snmp version - `{0}`".format(snmp_version)
        )

    errorIndication, errorStatus, errorIndex, varBinds = cmdgen.CommandGenerator().nextCmd(
        auth_data,
        cmdgen.UdpTransportTarget((host, snmp_port)),
        (1, 3, 6, 1, 2, 1, 4, 20, 1, 2),  # interface description
        (1, 3, 6, 1, 2, 1, 4, 20, 1, 1),  # interface IP
        (1, 3, 6, 1, 2, 1, 4, 20, 1, 3),  # interface netmask
    )

    print errorIndication, errorStatus, errorIndex, varBinds
    snmp_data = varBinds
    ips_raw = snmp_data
    interfaces_count = max([int(i[0][1]) for i in ips_raw])
    ethernet_info = ""

    for interface_number in xrange(1, interfaces_count + 1):
        snmp_data = __get_snmp_data(
            host,
            (1, 3, 6, 1, 2, 1, 2, 2, 1, 2, interface_number)
        )

        interface_name = str(snmp_data[0][1])
        interface_ip = [repr(i[1][1])[11:-2] for i in ips_raw if i[0][1] == interface_number]
        interface_netmask = [repr(i[2][1])[11:-2] for i in ips_raw if i[0][1] == interface_number]
        ethernet_info += interface_name + ": " + " ".join(["%s/%s\n" % (i, j) for i, j in zip(interface_ip, interface_netmask)]) + " "

    ethernet_info += "Default GW: {0}".format(GW_IP)
    return ethernet_info


@lightweightworker.cache_response("hosts", 3600)
def system_info_get(data, env, stop_monitor, session_info):
    """Get information about RTP hosts via SNMP: load of CPU, memory and hdd, information
    about ethernet interfaces and system info."""
    default_fields = [
        "host",  # String
        "system_info",  # String
        "cpu_info",  # String
        "memory_info",  # String
        "disk_info",  # String
        "ethernet_info",  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key('hosts', optional=True): trafaret.List(trafaret.String()),  # IP addresses of RTP hosts
                trafaret.Key("fields", optional=True, default=default_fields): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
            }
        ).ignore_extra("*")

        checked_data = data_validation.check(data)

    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        if not checked_data.get("hosts"):
            hosts = s.query(Host.host).filter_by(enabled=True).all()

            if not hosts:
                raise errors.BillingError(
                    status.NOT_ACCEPTABLE,
                    "No enabled hosts found."
                )

            checked_data["hosts"] = [h[0] for h in hosts]

        fields_map = {
            "host": lambda host: host,
            "system_info": _get_system_info,
            "cpu_info": _get_cpu_info,
            "memory_info": _get_memory_info,
            "disk_info": _get_disk_info,
            "ethernet_info": _get_ethernet_info,
        }

        with concurrent.futures.ThreadPoolExecutor(max_workers=15) as executor:
            def proceed_host(host):
                return [fields_map[field](host) for field in checked_data["fields"]]
            results = [res for res in executor.map(proceed_host, checked_data["hosts"])]

        return {'data': results, "fields": checked_data["fields"]}


__API__ = {
    "system_info_get": system_info_get,
}
