'''
    VERSION 0.13
'''
import os
import glob


PATH = os.path.dirname(os.path.abspath(__file__))


# The mapping of API functions names to their implementations.
# One must create a GlobalEnvironment instance and pass it
# to those callbacks with an appropriate session_info value
# on function call.
API = {}


for modname in glob.glob(os.path.join(PATH, "*.py")):
    modname = os.path.basename(modname)[:-3]  # split the extension from a module file name
    mod = __import__(modname, globals(), locals(), [], 1)  # 1 means "use relative imports"

    if hasattr(mod, "__API__"):
        API.update(mod.__API__)
