#!/usr/bin/python

import logging
import logging.handlers

import gearman
from flup.server.fcgi import WSGIServer
from gzip_middleware import Gzipper


tasks = {
    #accucore functions
    #TODO add here rest of them
    'tp_ips_get': 900.0,
    'op_ips_get': 900.0,
    'actual_ptices_get_ac': 900.0,
    'tps_get': 900.0,
    'ops_get': 900.0,
    'balances_get': 900.0,
    'payment_get': 900,
    'payments_get_ac': 900,


    'incoming_calls_get': 600.0,
    'outgoing_calls_get': 600.0,
    'fin_summary_get': 900.0,
    'op_call_path_analysis_get': 600.0,
}

# Init logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.handlers.RotatingFileHandler(
    filename='/var/log/lighttpd/mc_api_fcgi.log',
    maxBytes=(1024 * 1024 * 10),
    backupCount=10
)
formatter = logging.Formatter(
    '%(asctime)-15s %(levelname)s(%(filename)s +%(lineno)d): %(message)s'
)
handler.setFormatter(formatter)
log.addHandler(handler)

gm_client = gearman.GearmanClient(['127.0.0.1:4730'])


def app(environ, start_response):
    start_response(
        '200 OK',
        [
            ('Content-Type', 'application/json'),
            ('Access-Control-Allow-Origin', '*')
        ]
    )

    request = environ['wsgi.input'].read(int(environ['CONTENT_LENGTH'])).strip()
    uri = environ['REQUEST_URI'].strip()

    task_name = uri[uri.rfind('/') + 1:]
    timeout = tasks.get(task_name, 60.0)

    list_of_jobs = [dict(task=task_name, data=request)]
    log.info(str(list_of_jobs))
    submitted_requests = gm_client.submit_multiple_jobs(list_of_jobs, background=False, wait_until_complete=False)
    completed_requests = gm_client.wait_until_jobs_completed(submitted_requests, poll_timeout=timeout)

    for completed_job_request in completed_requests:
        if completed_job_request.state == 'COMPLETE':
            result = completed_job_request.result
            yield result
        else:
            yield "{\"status\": 500, \"message\": \"Function does not exist\"}"
            log.error(str(list_of_jobs))


WSGIServer(Gzipper(app)).run()
