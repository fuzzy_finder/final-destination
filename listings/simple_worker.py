#!/usr/bin/env python
# coding: utf-8

import trafaret

from billing import lightweightworker
from billing.model import Tp, Op, TpTarif, Dialpeer, RelDialpeerTp, OpTarif
from billing.model import get_entity_for_schema, Colocation, Client, Balance
from billing.model import Group, RelOpGroup, RelTpGroup, RelDialpeerGroup
from billing import errors, status, get_config
from billing.utils import fields_check, sort_by_entry_get

from pt_worker_orm import _dataupdate_tp, _dataupdate_tp_ctx
from pt_worker_orm import _dublicate_name_check, _add_point_to_history
from pt_worker_orm import _dataupdate_op, _dataupdate_op_ctx

from groups_orm_worker import _add_op_to_group, _dataupdate_origination_point, _dataupdate_route_table

config = get_config()
service_dp = int(config["db"]["service_dp"])

RDT_AUTO_WEIGHT = 0
RDT_AUTO_PRIOTITY = 0

service_group_suffix = "_group"

dilpeer_data = {
    "name": "Service_dialpeer_{0}",
    "dstregexp": "^{0}.*$",
    "priority": 0,
    "enabled": True,
    "stop": True,
    "denyregexp": "",
    "capacity": 1000,
    "delta_price": 0.0,
    "ani_regexp": "",
    "dnis_regexp": "",
}


@lightweightworker.invalidate_responses(["ops", "ops_groups"])
def op_add_simple(data, env, stop_monitor, session_info):
    """Creates OP in "Simple routing" mode and OP tarif with name [OP_name tarif]. Function
    raises error if user is trying assign point to invisible company."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("name"): trafaret.String(),  # OP name, should be unique
                trafaret.Key("description", optional=True, default=""): trafaret.String(allow_blank=True),  # point description
                trafaret.Key("client_id", optional=True): trafaret.Int(gt=0),  # company owner identifier in table billing.client. Client should be visible.
                trafaret.Key("balance_id", optional=True): trafaret.Int(gt=0),  # balance id of company. Company should be visible.
                trafaret.Key("invisible", optional=True, default=False): trafaret.Bool(),  # boolean value, if True then point is invisible and disabled
                trafaret.Key("allow_internal", optional=True, default=False): trafaret.Bool(),  # boolean option. If "true"  - allows calls to TPs which belongs same company as given OP
            }
        )
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        return _point_add(s, session_info, checked_data, Op, OpTarif, _dataupdate_op, _dataupdate_op_ctx)


@lightweightworker.invalidate_responses(["tps", "tps_groups"])
def tp_add_simple(data, env, stop_monitor, session_info):
    """Creates TP in "Simple routing" mode (add new entries in schema routing and all contexts),
    creates TP tarif with name [TP_name tarif] for the point and adds relation with service dialpeer in all
    contexts, if such it exists.

    Function raises error:
        1) if service dialpeer doesn't exist in context.
        2) if user is trying to assign TP to the invisible company."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("name"): trafaret.String(),  # TP name, should be unique.
                trafaret.Key("description", optional=True, default=""): trafaret.String(allow_blank=True),  # point description
                trafaret.Key("client_id", optional=True): trafaret.Int(gt=0),  # company owner identifier in table billing.client. Client should be visible.
                trafaret.Key("balance_id", optional=True): trafaret.Int(gt=0),  # company's balance id
                trafaret.Key("invisible", optional=True, default=False): trafaret.Bool(),  # boolean value, if True then point is invisible and disabled
            }
        )
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
        return _point_add(s, session_info, checked_data, Tp, TpTarif, _dataupdate_tp, _dataupdate_tp_ctx)


def _point_add(s, session_info, checked_data, point_type, tarif_type, on_point_add, on_point_ctx_add):
    #check point with such name in DB
    instance_name = "Termination point" if point_type == Tp else "Origination point"
    _dublicate_name_check(s, get_entity_for_schema(point_type, "routing"), checked_data, instance_name)

    if "balance_id" not in checked_data and 'client_id' not in checked_data:
        raise errors.BillingError(status.WRONG_INPUT, "'balance_id' or 'client_id' is required")

    if not "balance_id" in checked_data:
        balance = s.query(Balance) \
                   .filter_by(client_id=checked_data["client_id"]) \
                   .first()
    else:
        balance = s.query(Balance) \
                   .filter_by(id=checked_data["balance_id"]) \
                   .first()
        if balance is None:
            raise errors.BillingDBError("There is no Balance with id {0} in DB".format(checked_data["balance_id"]))

    # check if the client is invisible
    is_invisible = s.query(Client.invisible).filter_by(id=balance.client.id).scalar()
    if is_invisible:
        raise errors.BillingDBError("You can not add point to invisible client")

    #special `simple` feature
    tarif = tarif_type(name="{0} tarif".format(checked_data["name"]), currency_id=balance.currency.id)
    s.add(tarif)

    point = get_entity_for_schema(point_type, "routing")(
        name=checked_data["name"],
        description=checked_data["description"],
        balance=balance,
        tarif=tarif,
        invisible=checked_data["invisible"],

        client_id=balance.client.id,
        currency_id=balance.currency.id,
        ext_id=checked_data.get("ext_id", 0),
    )

    if point_type is Op:
        point.allow_internal = checked_data["allow_internal"]
    s.add(point)

    _add_point_to_history(s, point, session_info)
    on_point_add(s, point)

    for ctx in s.query(Colocation).all():
        point_ctx = get_entity_for_schema(point_type, ctx.namespace)(
            id=point.id,
            name=point.name,
            description=point.description,
            balance_id=point.balance.id,
            tarif_id=point.tarif.id,
            invisible=point.invisible,

            client_id=point.client_id,
            currency_id=point.currency_id,
            ext_id=point.ext_id
        )
        if point_type is Op:
            point_ctx.allow_internal = point.allow_internal
        s.add(point_ctx)

        on_point_ctx_add(s, point_ctx, ctx)

        if point_type is Tp:
            # special `simple` option
            RDT_ctx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)
            DP_ctx = get_entity_for_schema(Dialpeer, ctx.namespace)

            dp = s.query(DP_ctx)\
                  .filter(DP_ctx.name.ilike("Service_dialpeer_%"))\
                  .first()

            if dp is None:
                raise errors.BillingDBError(
                    "Service DP not found"
                )

            rel_dialpeer_tp = RDT_ctx(
                tp_id=point.id,
                dialpeer_id=dp.id,
                priority=RDT_AUTO_PRIOTITY,
                weight=RDT_AUTO_WEIGHT,
            )

            s.add(rel_dialpeer_tp)

            _dataupdate_route_table(s, ctx)

    return {"id": point.id}


@lightweightworker.invalidate_responses(["ops_groups", "tps_groups", "groups_dialpeers", "tps_dialpeers"])
def rel_op_tp_add(data, env, stop_monitor, session_info):
    """
    Creates relations between chosen OP and TP(s), i.e.:
        1) creates service dialpeer '.*' if such dialpeer doesn't exist in the given
        context,
        2) creates service group for given OP if such doesn't exist in the given
        context,
        3) creates relation between service dialpeer and given TP, if such doesn't
        exist in the chosen context,
        4) adds dialpeer, OP and TP to the service group of given OP in specified
        context.
        Function raises error if there is already exist relations:
           1) OP with groups,
           2) TP with dialpeers
           3) TP with groups
        created in advanced mode.
    """
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("op_ids"): trafaret.List(trafaret.Int(gt=0)),
                trafaret.Key("tp_ids"): trafaret.List(trafaret.Int(gt=0)),  # list of TP ids
                trafaret.Key("contexts"): trafaret.List(trafaret.Int(gt=0)),  # context ids to which relations will be added
            }
        )
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    for op_id in checked_data["op_ids"]:
        with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

            op_check = s.query(get_entity_for_schema(Op, "routing"))\
                        .filter_by(id=op_id)\
                        .first()
            if not op_check:
                raise errors.BillingDBError(
                    "There is no OP with id {0} in DB".format(op_id)
                )

            ctx_list = s.query(Colocation)
            if checked_data.get("contexts"):
                ctx_list = ctx_list.filter(Colocation.id.in_(checked_data["contexts"]))

            for ctx in ctx_list.all():
                GR_ctx = get_entity_for_schema(Group, ctx.namespace)

                RTG_ctx = get_entity_for_schema(RelTpGroup, ctx.namespace)
                ROG_ctx = get_entity_for_schema(RelOpGroup, ctx.namespace)
                RDG_ctx = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)
                RDT_ctx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)

                # check op-service group relation.
                # At the monent we suppose that sevice group consists with
                # op_name+prefix. TODO - find better solution to check it
                rog_check = s.query(ROG_ctx) \
                             .join(GR_ctx) \
                             .filter(ROG_ctx.op_id == op_id) \
                             .filter(GR_ctx.name != "{0}{1}".format(op_check.name, service_group_suffix))\
                             .first()
                if rog_check:
                    raise errors.BillingError(
                        status.NOT_ACCEPTABLE,
                        "Relation was created in advanced mode. You can't change it in simple mode."
                    )

                # check, is service group avialible for this OP
                rel_op_group = s.query(ROG_ctx) \
                                .join(GR_ctx) \
                                .filter(ROG_ctx.op_id == op_id) \
                                .filter(GR_ctx.name == "{0}{1}".format(op_check.name, service_group_suffix))\
                                .first()

                if not rel_op_group:
                    # add group here
                    group = GR_ctx(
                        name="{0}{1}".format(op_check.name, service_group_suffix),
                        internal=False
                    )
                    s.add(group)
                    _dataupdate_route_table(s, ctx)
                    s.commit()

                    # add group-op relation here
                    _add_op_to_group(s, ctx, op_id, group.id, 0)
                    s.commit()
                    _dataupdate_origination_point(s, ctx, op_id)
                    group_id = group.id
                else:
                    group_id = rel_op_group.group_id

                # check DP-group relation
                rel_dp_group = s.query(RDG_ctx)\
                                .filter_by(dialpeer_id=service_dp)\
                                .filter_by(group_id=group_id)\
                                .first()

                if not rel_dp_group:
                    # add DP-group relation
                    rel_dialpeer_group = RDG_ctx(
                        group_id=group_id,
                        dialpeer_id=service_dp
                    )
                    s.add(rel_dialpeer_group)

                    _dataupdate_route_table(s, ctx)

                for tp_id in checked_data["tp_ids"]:
                    # check relation tp-gtoup from advanced mode
                    adv_groups = s.query(RDG_ctx.group_id)\
                                  .filter(RDG_ctx.dialpeer_id != service_dp)\

                    rtg_check = s.query(RTG_ctx)\
                                 .filter(RTG_ctx.group_id.in_(adv_groups))\
                                 .filter(RTG_ctx.tp_id == tp_id)\
                                 .first()

                    if rtg_check:
                        raise errors.BillingError(
                            status.NOT_ACCEPTABLE,
                            "Relation was created in advanced mode. You can't change it in simple mode."
                        )

                    # check relation dialpeer-tp from advanced mode
                    rdt_check = s.query(RDT_ctx)\
                                 .filter(RDT_ctx.dialpeer_id != service_dp)\
                                 .filter(RDT_ctx.tp_id == tp_id)\
                                 .first()

                    if rdt_check:
                        raise errors.BillingError(
                            status.NOT_ACCEPTABLE,
                            "Relation was created in advanced mode. You can't change it in simple mode."
                        )

                    # check TP-group relation
                    rel_tp_group = s.query(RTG_ctx)\
                                    .filter_by(group_id=group_id)\
                                    .filter_by(tp_id=tp_id)\
                                    .first()

                    if rel_tp_group is None:
                        rel_tp_group = RTG_ctx(
                            tp_id=tp_id,
                            group_id=group_id
                        )
                        s.add(rel_tp_group)

                    # check TP-dp relation
                    rel_tp_dp = s.query(RDT_ctx)\
                                 .filter_by(dialpeer_id=service_dp)\
                                 .filter_by(tp_id=tp_id)\
                                 .first()
                    if not rel_tp_dp:
                        rel_tp_dp = RDT_ctx(
                            tp_id=tp_id,
                            dialpeer_id=service_dp,
                            priority=0,
                            weight=0,
                        )

                        s.add(rel_tp_dp)
                        _dataupdate_route_table(s, ctx)

    return {}


@lightweightworker.invalidate_responses(["ops_groups", "tps_groups", "groups_dialpeers", "tps_dialpeers"])
def rel_op_tp_del(data, env, stop_monitor, session_info):
    """Removes relations between chosen OP and TPs, i.e. removes relations between TPs and service group of given OP
    and service dialpeer in the chosen context(s). If given OP has lost all relation with TPs then service group of
    the OP will be removed too."""
    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("op_ids"): trafaret.List(trafaret.Int(gt=0)),
                trafaret.Key("tp_ids"): trafaret.List(trafaret.Int(gt=0)),  # TP ids which need remove from relations with given OP
                trafaret.Key("contexts"): trafaret.List(trafaret.Int(gt=0)),  # context ids in which relation need to delete
            }
        )
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    # FIXME
    assert len(checked_data["contexts"]) == 1

    for op_id in checked_data["op_ids"]:
        with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
            op_check = s.query(get_entity_for_schema(Op, "routing")).get(op_id)
            if op_check is None:
                raise errors.BillingDBError("There is no OP with id {0} in DB".format(op_id))

            for tp_id in checked_data["tp_ids"]:
                tp_check = s.query(get_entity_for_schema(Tp, "routing")).get(tp_id)
                if tp_check is None:
                    raise errors.BillingDBError("There is no TP with id {0} in DB".format(tp_id))

                ctx = s.query(Colocation)\
                       .filter(Colocation.id.in_(checked_data["contexts"]))\
                       .first()

                GR_ctx = get_entity_for_schema(Group, ctx.namespace)
                RTG_ctx = get_entity_for_schema(RelTpGroup, ctx.namespace)
                ROG_ctx = get_entity_for_schema(RelOpGroup, ctx.namespace)
                RDG_ctx = get_entity_for_schema(RelDialpeerGroup, ctx.namespace)
                RDT_ctx = get_entity_for_schema(RelDialpeerTp, ctx.namespace)

                dp_ids = s.query(RDT_ctx.dialpeer_id) \
                          .filter(RDT_ctx.tp_id == tp_id)

                if any([d[0] != service_dp for d in dp_ids.all()]):
                    raise errors.BillingError(
                        status.NOT_ACCEPTABLE,
                        "Relation was created in advanced mode."
                    )

                # check group for this OP
                op_group_id = s.query(ROG_ctx.group_id) \
                               .join(GR_ctx) \
                               .filter(ROG_ctx.op_id == op_id) \
                               .filter(GR_ctx.name == "{0}{1}".format(op_check.name, service_group_suffix))\
                               .scalar()

                group_id = op_group_id
                dialpeer_id = service_dp

                s.query(RTG_ctx) \
                 .filter_by(tp_id=tp_id) \
                 .filter_by(group_id=group_id) \
                 .delete()

                s.query(RDT_ctx) \
                 .filter_by(tp_id=tp_id) \
                 .filter_by(dialpeer_id=service_dp) \
                 .delete()

                _dataupdate_origination_point(s, ctx, op_id)
                _dataupdate_route_table(s, ctx)
                s.commit()
        # if we got no more tp related with this OP we must
        # remove service group
        related_tps = tps_op_get(
            {
                "op_id": op_id,
                "contexts": checked_data["contexts"],
                "limit": 2
            },
            env, stop_monitor, session_info
        )["data"]

        if not related_tps:
            with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:
                s.query(RDG_ctx) \
                 .filter_by(dialpeer_id=dialpeer_id) \
                 .filter_by(group_id=group_id) \
                 .delete()

                s.query(GR_ctx).filter_by(id=group_id).delete()
                s.commit()

                ctx = s.query(Colocation)\
                       .filter(Colocation.id.in_(checked_data["contexts"]))\
                       .first()

                _dataupdate_origination_point(s, ctx, op_id)
                _dataupdate_route_table(s, ctx)

    return {}


def tps_op_get(data, env, stop_monitor, session_info):
    """Returns a list of TPs which are connected with given OP in simple and advanced routing modes, i.e.
    termination points that satisfy condition:
        Points and dialpeers connected with them must belong to the same group as OP in the chosen context."""
    default_fields = [
        "id",  # Int
        "name",  # String
        "priority",
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("op_id"): trafaret.Int(gt=0),  # filter by OP id
                trafaret.Key("name", optional=True): trafaret.String(allow_blank=True),  # filter by OP name
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # contexts ids where search OP-TP relations
                trafaret.Key("fields", default=default_fields): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which result will be sorted. Result can be sorted ascending or descending.
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # says how many rows will be in response
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0)  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra("*")
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        fields_map = {
            "id": "rtg.tp_id",
            "name": "tp.name",
            "priority": "kk.priority"
        }

        query = 'SELECT DISTINCT ' + ','.join([fields_map[f] for f in checked_data['fields']])
        query += """ FROM {0}.rel_op_group rog
            INNER JOIN {0}.rel_tp_group rtg on rtg.group_id=rog.group_id
            INNER JOIN
            (SELECT distinct rdt.tp_id, rdt.priority FROM
                {0}.rel_op_group rog
                inner join {0}.rel_dialpeer_group rdg on rdg.group_id=rog.group_id
                inner join {0}.rel_dialpeer_tp rdt on rdt.dialpeer_id=rdg.dialpeer_id
                WHERE rog.op_id=:op_id) kk ON rtg.tp_id=kk.tp_id
             INNER JOIN {0}.tp ON kk.tp_id=tp.id
             WHERE rog.op_id=:op_id
            """

        if checked_data.get("name"):
            query += " AND lower(tp.name) LIKE lower(:name) || '%%' "

        if session_info.get("companies", []):
            query += " AND tp.client_id IN (" + ','.join([str(x) for x in session_info['companies']]) + ") "

        query += " ORDER BY name"""

        contexts = s.query(Colocation)
        if checked_data["contexts"]:
            contexts = contexts.filter(Colocation.id.in_(checked_data["contexts"]))

        results = []
        for ctx in contexts.all():
            pre_results = s.execute(query.format(ctx.namespace), checked_data)
            results.extend([list(i) for i in pre_results.fetchall()])

        return {"fields": checked_data["fields"], "data": results}


def tps_free_op_get(data, env, stop_monitor, session_info):
    """Returns list of TPs which don't connected with chosen OP, i.e. all termination points that
    don't belong service group of the OP, in the chosen context(s)."""
    default_fields = [
        "id",  # Int
        "name",  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("op_id"): trafaret.Int(gt=0),
                trafaret.Key("name", optional=True): trafaret.String(allow_blank=True),  # OP name
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # contexts ids
                trafaret.Key("fields", default=default_fields): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which result will be sorted. Result can be sorted ascending or descending.
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # says how many rows will be in response
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0)  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra("*")
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        fields_map = {
            "id": "tp.id",
            "name": "tp.name"
        }

        query = 'SELECT ' + ','.join([fields_map[f] for f in checked_data['fields']])
        query += """ FROM {0}.tp
                WHERE tp.id NOT IN
                (SELECT distinct rtg.tp_id
                FROM {0}.rel_op_group rog
                INNER JOIN {0}.rel_tp_group rtg on rtg.group_id=rog.group_id
                INNER JOIN
                (SELECT distinct rdt.tp_id FROM
                    {0}.rel_op_group rog
                    inner join {0}.rel_dialpeer_group rdg on rdg.group_id=rog.group_id
                    inner join {0}.rel_dialpeer_tp rdt on rdt.dialpeer_id=rdg.dialpeer_id
                    WHERE rog.op_id=:op_id) kk ON rtg.tp_id=kk.tp_id
                WHERE rog.op_id=:op_id )
                AND tp.invisible=false
            """

        if checked_data.get("name"):
            query += " AND lower(tp.name) LIKE lower(:name) || '%%' "

        if session_info.get("companies", []):
            query += " AND tp.client_id IN (" + ','.join([str(x) for x in session_info['companies']]) + ") "

        query += " ORDER BY name"""

        contexts = s.query(Colocation)
        if checked_data["contexts"]:
            contexts = contexts.filter(Colocation.id.in_(checked_data["contexts"]))

        results = []
        for ctx in contexts.all():
            pre_results = s.execute(query.format(ctx.namespace), checked_data)
            results.extend([list(i) for i in pre_results.fetchall()])

        return {"fields": checked_data["fields"], "data": results}


def ops_tp_get(data, env, stop_monitor, session_info):
    """Returns a list of OPs which are connected with given TP in simple and advanced routing modes, i.e.
    OPs that satisfy condition:
        Points and dialpeers connected with them must belong to the same group as TP in the chosen context."""
    default_fields = [
        "id",  # Int
        "name",  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_id"): trafaret.Int(gt=0),  # filter by OP id
                trafaret.Key("name", optional=True): trafaret.String(allow_blank=True),  # filter by OP name
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # contexts ids where search OP-TP relations
                trafaret.Key("fields", default=default_fields): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which result will be sorted. Result can be sorted ascending or descending.
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # says how many rows will be in response
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0)  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra("*")
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        fields_map = {
            "id": "rog.op_id",
            "name": "op.name"
        }

        query = 'SELECT DISTINCT ' + ','.join([fields_map[f] for f in checked_data['fields']])
        query += """ FROM {0}.rel_op_group rog
        INNER JOIN {0}.op ON rog.op_id=op.id
        WHERE rog.group_id IN (
            select distinct group_id from {0}.rel_dialpeer_group rdg
            where dialpeer_id IN (select dialpeer_id from {0}.rel_dialpeer_tp where tp_id=:tp_id)
           and group_id in (select group_id from {0}.rel_tp_group where tp_id=:tp_id)
        )
        """

        if checked_data.get("name"):
            query += " AND lower(op.name) LIKE lower(:name) || '%%' "
        if session_info.get("companies", []):
            query += " AND op.client_id IN (" + ','.join([str(x) for x in session_info['companies']]) + ") "
        query += " ORDER BY name"""

        contexts = s.query(Colocation)
        if checked_data["contexts"]:
            contexts = contexts.filter(Colocation.id.in_(checked_data["contexts"]))

        results = []
        for ctx in contexts.all():
            pre_results = s.execute(query.format(ctx.namespace), checked_data)
            results.extend([list(i) for i in pre_results.fetchall()])

        return {"fields": checked_data["fields"], "data": results}


def ops_free_tp_get(data, env, stop_monitor, session_info):
    """Returns list of TPs which don't connected with chosen OP, i.e. all termination points that
    don't belong service group of the OP, in the chosen context(s)."""
    default_fields = [
        "id",  # Int
        "name",  # String
    ]

    try:
        data_validation = trafaret.Dict(
            {
                trafaret.Key("tp_id"): trafaret.Int(gt=0),
                trafaret.Key("name", optional=True): trafaret.String(allow_blank=True),  # OP name
                trafaret.Key("contexts", default=[]): trafaret.List(trafaret.Int()),  # contexts ids
                trafaret.Key("fields", default=default_fields): trafaret.List(trafaret.String())
                >> (lambda fields: fields_check(fields, default_fields)),  # required fields. If absent - takes fields specified in "default fields". If empty - returns error.
                trafaret.Key("sort_by", default=[], optional=True): sort_by_entry_get(default_fields),  # field on which result will be sorted. Result can be sorted ascending or descending.
                trafaret.Key("limit", default=0, optional=True): trafaret.Int(gte=0),  # says how many rows will be in response
                trafaret.Key("offset", default=0, optional=True): trafaret.Int(gte=0)  # Offset says to skip that many rows before beginning to return rows.
            }
        ).ignore_extra("*")
        checked_data = data_validation.check(data)
    except trafaret.DataError as e:
        raise errors.BillingError(
            status.WRONG_INPUT,
            ", ".join(["{0}: {1}".format(k, v) for k, v in e.as_dict().items()])
        )

    with env.get_db_session(automatic_audit=True, audit_session_info=session_info) as s:

        fields_map = {
            "id": "op.id",
            "name": "op.name"
        }

        query = 'SELECT ' + ','.join([fields_map[f] for f in checked_data['fields']])
        query += """ FROM {0}.op
            WHERE op.id NOT IN
            (select distinct rog.op_id
            FROM {0}.rel_op_group rog
            WHERE group_id IN (
                select distinct group_id from {0}.rel_dialpeer_group rdg
                where dialpeer_id IN (select dialpeer_id from {0}.rel_dialpeer_tp where tp_id=:tp_id)
                and group_id in (select group_id from {0}.rel_tp_group where tp_id=:tp_id)
            )
        ) """

        if checked_data.get("name"):
            query += " AND lower(op.name) LIKE lower(:name) || '%%' "

        if session_info.get("companies", []):
            query += " AND op.client_id IN (" + ','.join([str(x) for x in session_info['companies']]) + ") "

        query += " ORDER BY name"""

        contexts = s.query(Colocation)
        if checked_data["contexts"]:
            contexts = contexts.filter(Colocation.id.in_(checked_data["contexts"]))

        results = []
        for ctx in contexts.all():
            pre_results = s.execute(query.format(ctx.namespace), checked_data)
            results.extend([list(i) for i in pre_results.fetchall()])

        return {"fields": checked_data["fields"], "data": results}

__API__ = {
    "rel_op_tp_add": rel_op_tp_add,
    "rel_op_tp_del": rel_op_tp_del,
    "tps_op_get": tps_op_get,
    "tps_free_op_get": tps_free_op_get,
    "op_add_simple": op_add_simple,
    "tp_add_simple": tp_add_simple,
    "ops_tp_get": ops_tp_get,
    "ops_free_tp_get": ops_free_tp_get,
}
