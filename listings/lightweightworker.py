import sys
import time
import threading
import traceback
import json
import functools
import datetime
import cPickle

import gearman

import butils
import status
import errors


class UtilThread(threading.Thread):
    """
        Helper thread which monitors the task supervisor to determine the moment
        when the current task should be stopped (in order to shutdown gracefully
        a supplied callback will be called which should stop the task)
    """

    def __init__(self, gEnv, job_id, callback):
        super(UtilThread, self).__init__()

        self.gm = gearman.GearmanClient(gEnv.gearman_hosts)
        self.job_id = job_id
        self.callback = callback
        self.terminate = False

    def run(self):
        while not self.terminate:
            job = self.gm.submit_job("util_should_stop", self.job_id, wait_until_complete=True)

            if int(job.result) == 1:
                self.callback()
                break
            else:
                time.sleep(0.5)


class LightWeightWorker(gearman.GearmanWorker):
    """
        A Gearman requests dispatcher
    """

    def __init__(self, gEnv):
        super(LightWeightWorker, self).__init__(gEnv.gearman_hosts)

        self.gEnv = gEnv

    def on_job_execute(self, current_job):
        """
            Dispatch the Gearman request to a registered callback

            Each callback should have the following signature:

                def callback(data, gEnv, shutdown_monitor_thread, session_info):
                    pass

            where:
                data                  -  passed method params
                gEnv                  -  is an instance of global environment
                shutdown_monitor_cls  -  a class of a thread which monitors the supervisor
                                         worker and calls a given callback when the worker's
                                         job should be stopped
                session_info          -  a dict which contains session id, user login, ip address, etc

            All callbacks are registered by means of register_task(task_name, callback)
            instance method call.
        """

        job_result = {
            "status": status.OK,
            "message": status.get_status(status.OK)
        }

        try:
            # parse input data
            try:
                current_job.data = json.loads(current_job.data)
            except ValueError as e:
                self.gEnv.get_logger().error(e)
                self.gEnv.get_logger().debug("JSON received: {0}".format(current_job.data))
                raise errors.BillingError(status.WRONG_INPUT)

            function_callback = self.worker_abilities[current_job.task]

            # check auth and permissions if needed
            needs_auth = not getattr(function_callback, "doesnt_need_auth", False)

            if needs_auth:
                session_info = self.check_session(current_job.data)

                # TODO: permission checking is intentionally turned off until good times finally come
                #self.check_permissions(current_job.task, current_job.data)
            else:
                session_info = {}

            # get method params
            if needs_auth:
                method_data = current_job.data.get("data")
                if method_data is None:
                    raise errors.BillingError(status.WRONG_INPUT, "You haven't passed the `data` field")
            else:
                method_data = current_job.data

            # dispatch the method call
            util_thread = functools.partial(UtilThread, self.gEnv, current_job.unique)

            started = datetime.datetime.now()
            self.gEnv.get_logger().info("Task '{0}' started".format(current_job.task))
            job_result.update(function_callback(method_data, self.gEnv, util_thread, session_info))
            self.gEnv.get_logger().info("Task '{0}' ended".format(current_job.task))
            ended = datetime.datetime.now()

            time_elapsed = ended - started
            time_elapsed_sec = (time_elapsed.microseconds + time_elapsed.seconds * 10. ** 6) / 10 ** 6
            job_result.update({"time_elapsed": time_elapsed_sec})
        except Exception as e:
            msg = "Task '{task}': {error}".format(
                task=current_job.task,
                error="".join(traceback.format_exception(*sys.exc_info()))
            )

            self.gEnv.get_logger().error(msg)
            job_result.update(errors.format_error_msg(e))
        finally:
            with self.gEnv._smaker_lock:
                if hasattr(self.gEnv, "sessionmaker"):
                    self.gEnv.sessionmaker.remove()

        return self.on_job_complete(current_job, json.dumps(job_result, cls=butils.BillingEncoder))

    def check_session(self, data):
        """
            Check if the user has correctly signed in and passed the session id
            Return the session info (id, user login, ip address, etc)
        """

        cache = self.gEnv.get_cache_connection()

        sid = data.get("session_id", None)
        if sid is None:
            raise errors.BillingError(status.MISSED_PARAMETER, "session_id is missing")

        sid_cache_key = "ses_id_{0}".format(sid)

        sid_data = cache.get(sid_cache_key)
        if sid_data is None:
            raise errors.BillingAuthError("session_id is invalid")

        # don't set TTL for special "infinite" sessions
        if cache.ttl(sid_cache_key) != -1:
            cache.expire(sid_cache_key, self.gEnv.session_ttl)

        return json.loads(sid_data)

def doesnt_need_auth(f):
    """
        A decorator which sets a doesnt_need_auth attribute on the given function

        Use this decorator to inform the LightWeightWorker that given method
        DOES NOT require the caller to authenticate first
    """

    f.doesnt_need_auth = True
    return f


def cache_response(realm, ttl=-1, update_ttl=True):
    """
        Caches the response of a decorated function in the given realm

        Args:
            realm  --- a set of cache keys that must be invalidated when a certain
                       entity is created/updated/deleted to ensure consistency between
                       the cache and DB.
            ttl    --- a number of seconds the cached result will expire in
                       (pass -1 to create a permanent cache entry)
            update_ttl --- a boolean flag that shows if the cache entry TTL
                           should be updated on each cache hit

        Usage:
            @lightweightworker.cache_response("tps", 3600)
            def tps_get(data, env, stop_monitor, session_info):
                pass
    """

    def decorator(f):
        @functools.wraps(f)
        def wrapper(data, gEnv, thread, session_info):
            cache = gEnv.get_cache_connection()

            key = "cache::{0}::{1}::{2}::{3}::{4}".format(
                realm,
                f.__name__,
                cPickle.dumps(data),
                cPickle.dumps(session_info.get("companies", [])),
                cPickle.dumps(session_info.get("show_ip", False)),
            )
            if cache.exists(key):
                if update_ttl:
                    cache.expire(key, ttl)
                return cPickle.loads(cache.get(key))

            rv = f(data, gEnv, thread, session_info)
            cache.set(key, cPickle.dumps(rv, 1))
            cache.expire(key, ttl)
            cache.sadd("cache::{0}".format(realm), key)
            return rv

        return wrapper

    return decorator


def invalidate_responses(realms):
    """
        Invalidates cached responses in given realms

        Args:
            realms --- is a list of sets of cache keys that must be invalidated when a certain
                       entity is created/updated/deleted to ensure consistency between the cache and DB.

        Usage:
            @lightweightworker.invalidate_responses(["tps"])
            def tp_add_or_update(data, env, stop_monitor, session_info):
                pass
    """

    assert isinstance(realms, list)

    def decorator(f):
        @functools.wraps(f)
        def wrapper(data, gEnv, thread, session_info):
            cache = gEnv.get_cache_connection()

            p = cache.pipeline()
            for realm in realms:
                realm_key = "cache::{0}".format(realm)

                keys = cache.smembers(realm_key)
                for k in keys:
                    cache.delete(k)
                    cache.srem(realm_key, k)
            p.execute()

            return f(data, gEnv, thread, session_info)

        return wrapper

    return decorator
